<?php

use Illuminate\Database\Seeder;

class ArrondissementsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('arrondissements')->insert([
            'name' => '1er Arrondissement',
            'slug' => '1er-arrondissement',
        ]);

        DB::table('arrondissements')->insert([
            'name' => '2e Arrondissement',
            'slug' => '2e-arrondissement',
        ]);

        DB::table('arrondissements')->insert([
            'name' => '3e Arrondissement',
            'slug' => '3e-arrondissement',
        ]);

    }
}
