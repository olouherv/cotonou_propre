<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UtilisateursTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('utilisateurs')->insert([
            'role_id' => '1',
            'name' => 'B. Hervé',
            'username' => 'herv',
            'email' => 'olouherv@gmail.com',
            'password' => bcrypt('herv'),
        ]);

        DB::table('utilisateurs')->insert([
            'role_id' => '1',
            'name' => 'Z. Harris',
            'username' => 'harris',
            'email' => 'harris@gmail.com',
            'password' => bcrypt('harris'),
        ]);

        DB::table('utilisateurs')->insert([
            'role_id' => '1',
            'name' => 'H. Indrid',
            'username' => 'indrid',
            'email' => 'indrid@gmail.com',
            'password' => bcrypt('indrid'),
        ]);

    }
}
