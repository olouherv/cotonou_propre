<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TypeloiTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('Typelois')->insert([
            'name' => 'Decret',
            'slug' => 'decret',
        ]);
    }
}
