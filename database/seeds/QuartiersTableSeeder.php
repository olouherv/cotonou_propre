<?php

use Illuminate\Database\Seeder;

class QuartiersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('quartiers')->insert([
            'name' => 'Dandji',
            'slug' => 'dandji',
            'arrondissement_id' => '1'
        ]);

        DB::table('quartiers')->insert([
            'name' => 'Donaten',
            'slug' => 'donaten',
            'arrondissement_id' => '1'
        ]);

        DB::table('quartiers')->insert([
            'name' => 'Ahouassa',
            'slug' => 'ahouassa',
            'arrondissement_id' => '2'
        ]);

        DB::table('quartiers')->insert([
            'name' => 'Irédé',
            'slug' => 'irede',
            'arrondissement_id' => '2'
        ]);

        DB::table('quartiers')->insert([
            'name' => 'Agbato',
            'slug' => 'agbato',
            'arrondissement_id' => '3'
        ]);

        DB::table('quartiers')->insert([
            'name' => 'Fifatin',
            'slug' => 'fifatin',
            'arrondissement_id' => '3'
        ]);
    }
}
