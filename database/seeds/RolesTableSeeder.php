<?php

use Illuminate\Database\Seeder;
use \Illuminate\Support\Facades\DB;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            'name' => 'Administrateur',
            'slug' => 'administrateur',
        ]);

        DB::table('roles')->insert([
            'name' => 'Responsable',
            'slug' => 'responsable',
        ]);

        DB::table('roles')->insert([
            'name' => 'Association',
            'slug' => 'association',
        ]);

        DB::table('roles')->insert([
            'name' => 'Citoyen',
            'slug' => 'citoyen',
        ]);

    }
}
