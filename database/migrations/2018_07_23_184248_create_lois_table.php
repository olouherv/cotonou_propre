<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLoisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lois', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('typeloi_id');
            $table->integer('utilisateur_id');
            $table->integer('zone_id')->default(0);
            $table->string('titre');
            $table->string('description')->nullable();
            $table->string('pdfurl');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lois');
    }
}
