<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePublicationUtilisateurTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('publication_utilisateur', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('utilisateur_id')->unsigned();
            $table->integer('publication_id')->unsigned();

            $table->foreign('publication_id')
                ->references('id')->on('publications')
                ->onDelete('cascade');

            $table->foreign('utilisateur_id')
                ->references('id')->on('utilisateurs')
                ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('utilisateur_publication');
    }
}
