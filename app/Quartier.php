<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Quartier extends Model
{
    //
    public function arrondissement(){
        return $this->belongsTo('App\Arrondissement');
    }

    public function responsable(){
        return $this->belongsTo('App\Utilisateur');
    }

    public function zones(){
        return $this->hasMany('App\Zone');
    }

    public function publications(){
        return $this->hasMany('App\Publication');
    }
}
