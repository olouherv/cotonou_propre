<?php

namespace App\Http\Controllers;

use App\Commentaire;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CommentaireController extends Controller
{
    //
    public function store(Request $request,$id){
        $this->validate($request,[
            'commentaire' => 'required',
        ]);
        $commentaire = new Commentaire();
        $commentaire->publication_id = $id;
        $commentaire->utilisateur_id = Auth::id();
        $commentaire->commentaire = $request->commentaire;
        $commentaire->save();
        Toastr::success('Commentaire bien enregistré.','Scucès');
        return redirect()->back();
    }

    public function commentaires(){
        $commentaires = Commentaire::latest()->get();
        return view('dashboard.voscommentaires.index',compact('commentaires'));

    }

    public function voscommentaires(){
        $commentaires = Commentaire::where('utilisateur_id',Auth::id())->orderBy('id','desc')->get();
        return view('dashboard.voscommentaires.index',compact('commentaires'));
    }

    public function destroy($id){
        $commentaire = Commentaire::find($id);
        $commentaire->forceDelete();
        Toastr::success('Commentaire bien supprimé.','Scucès');
        return redirect()->back();
    }

}
