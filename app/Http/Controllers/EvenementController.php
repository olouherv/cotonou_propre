<?php

namespace App\Http\Controllers;

use App\Arrondissement;
use App\Evenement;
use App\Typeevenement;
use App\Typepublication;
use Illuminate\Http\Request;

class EvenementController extends Controller
{
    //
    public function index($type=null){
        $typeevenements = Typeevenement::orderBy('name','asc')->get();
        $arrondissements = Arrondissement::orderBy('id','asc')->get();
        if(isset($type)){
            $evenements = Evenement::whereHas('typeevenement',function ($query) use ($type){
                $query->where('slug',$type);
            })->orderBy('datedebut','asc')->paginate(6);
        }else{
            $evenements = Evenement::orderBy('datedebut','desc')->paginate(6);
        }
        return view('evenements',compact('evenements','typeevenements','arrondissements'));
    }

    public function arrondissement($slug)
    {
        $typeevenements = Typeevenement::orderBy('name','asc')->get();
        $arrondissements = Arrondissement::orderBy('id','asc')->get();

        $evenements = Evenement::whereHas('zone.quartier.arrondissement', function ($query) use ($slug){
            $query->where('slug',$slug);
        })->orderBy('datedebut','desc')->paginate(6);

        return view('evenements',compact('evenements','typeevenements','arrondissements'));
    }
}
