<?php

namespace App\Http\Controllers\Dashboard;

use App\Arrondissement;
use App\Quartier;
use App\Utilisateur;
use App\Zone;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ZoneController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $zones = Zone::orderBy('quartier_id','asc')->orderBy('name','asc')->get();
        return view('dashboard.zone.index',compact('zones'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $arrondissements = Arrondissement::all();
        $quartiers = Quartier::orderBy('arrondissement_id','asc')->orderBy('name','asc')->get();
        $users = Utilisateur::all()->where('role_id','=','2');
        return view('dashboard.zone.create',compact('users','quartiers', 'arrondissements'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request,[
            'name' => 'required',
            'quartier_id' => 'required',
        ]);
        $zone = new Zone();
        $zone->name = $request->name;
        $zone->quartier_id = $request->quartier_id;
        $zone->responsable_id = $request->responsable_id;
        $zone->slug = str_slug($request->name);
        $zone->save();
        Toastr::success('Zone bien enregistré', 'Succès');
        return redirect()->route('dashboard.zone.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $arrondissements = Arrondissement::all();
        $zone = Zone::find($id);
        $users = Utilisateur::all()->where('role_id','=','2');
        $quartiers = Quartier::orderBy('arrondissement_id','asc')->orderBy('name','asc')->get();
        return view('dashboard.zone.edit', compact('zone','users','quartiers','arrondissements'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $this->validate($request,[
            'name' => 'required',
            'quartier_id' => 'required',
        ]);

        $zone = Zone::find($id);
        $zone->name = $request->name;
        $zone->quartier_id = $request->quartier_id;
        $zone->responsable_id = $request->responsable_id;
        $zone->slug = str_slug($request->name);
        $zone->save();

        Toastr::success('Zone bien modifié', 'Succès');
        return redirect()->route('dashboard.zone.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Zone::find($id)->delete();
        Toastr::success('Zone bien supprimé', 'Succès');
        return redirect()->back();
    }
}
