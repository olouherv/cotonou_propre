<?php

namespace App\Http\Controllers\Dashboard;

use App\Typeloi;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Brian2694\Toastr\Facades\Toastr;
class TypeloiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $types = Typeloi::latest()->get();
        return view('dashboard.typeloi.index',compact('types'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('dashboard.typeloi.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request,[
            'name' => 'required'
        ]);
        $type = new Typeloi();
        $type->name = $request->name;
        $type->slug = str_slug($request->name);
        $type->save();
        Toastr::success('Type de loi bien enregistré', 'Succès');
        return redirect()->route('dashboard.typeloi.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $type = Typeloi::find($id);
        return view('dashboard.typeloi.edit', compact('type'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $this->validate($request,[
            'name' => 'required'
        ]);

        $type = Typeloi::find($id);
        $type->name = $request->name;
        $type->slug = str_slug($request->name);
        $type->save();

        Toastr::success('Type de loi bien modifié', 'Succès');
        return redirect()->route('dashboard.typeloi.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Typeloi::find($id)->delete();
        Toastr::success('Type de loi bien supprimé', 'Succès');
        return redirect()->back();
    }
}
