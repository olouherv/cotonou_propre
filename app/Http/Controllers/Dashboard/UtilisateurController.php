<?php

namespace App\Http\Controllers\Dashboard;

use App\Role;
use App\Utilisateur;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class UtilisateurController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $utilisateurs = Utilisateur::orderBy('role_id','asc')->get();
        return view('dashboard.utilisateurs.index',compact('utilisateurs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $roles = Role::all();
        return view('dashboard.utilisateurs.create',compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request,[
            'name' => 'required',
            'username' => 'required|unique:utilisateurs',
            'email' => 'required|unique:utilisateurs',
            'role_id' => 'required',
            'password' => 'required|min:6',
        ]);

        if($request->role_id == 3 && !isset($request->responsable)){
            Toastr::error("Veuillez entrer le nom du responsable de l'association",'Erreur');
            return redirect()->back();
        }

        $utilisateur = new Utilisateur();
        $utilisateur->name = $request->name;
        $utilisateur->email = $request->email;
        $utilisateur->username = $request->username;
        $utilisateur->role_id = $request->role_id;
        $utilisateur->password = Hash::make($request->password);
        $utilisateur->telephone = $request->telephone;
        $utilisateur->responsable = $request->responsable;
        $utilisateur->about = $request->about;
        $utilisateur->save();

        Toastr::success('Utilisateur bien ajouté','Succès');
        return redirect()->route('dashboard.utilisateurs.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $utilisateur = Utilisateur::find($id);
        $roles = Role::all();
        return view('dashboard.utilisateurs.edit',compact('utilisateur','roles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $utilisateur = Utilisateur::find($id);

        if($request->username != $utilisateur->username){
            $this->validate($request,[
                'username' => 'required|unique:utilisateurs'
            ]);
        }

        if($request->email != $utilisateur->email){
            $this->validate($request,[
                'email' => 'required|unique:utilisateurs'
            ]);
        }

        $this->validate($request,[
            'name' => 'required',
            'role_id' => 'required',
        ]);

        if($request->role_id == 3 && !isset($request->responsable)){
            Toastr::error("Veuillez entrer le nom du responsable de l'association",'Erreur');
            return redirect()->back();
        }

        $utilisateur->name = $request->name;
        $utilisateur->email = $request->email;
        $utilisateur->username = $request->username;
        $utilisateur->role_id = $request->role_id;
        if(isset($request->password)){
            $utilisateur->password = Hash::make($request->password);
        }
        $utilisateur->telephone = $request->telephone;
        $utilisateur->responsable = $request->responsable;
        $utilisateur->about = $request->about;
        $utilisateur->save();

        Toastr::success('Utilisateur bien modifié','Succès');
        return redirect()->route('dashboard.utilisateurs.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $utilisateur = Utilisateur::find($id);
        $utilisateur->forceDelete();
        Toastr::success('Utilisateur supprimé avec succès','Succès');
        return redirect()->back();
    }
}
