<?php

namespace App\Http\Controllers\Dashboard;

use App\Arrondissement;
use App\Evenement;
use App\Typeevenement;
use App\Zone;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class EvenementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $evenements = Evenement::latest()->get();
        return view('dashboard/evenement/index',compact('evenements'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $zones = Zone::all()->sortBy('quartier_id');
        $arrondissements = Arrondissement::all();
        $types = Typeevenement::all()->sortBy('name');
        return view('dashboard/evenement/create',compact('zones','arrondissements','types'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request,[
            'titre' => 'required|max:100',
            'description' => 'required',
            'typeevenement_id' => 'required',
            'zone_id' => 'required',
            'datedebut' => 'required',
            'datefin' => 'required',
        ]);

        $evenement = new Evenement();

        $evenement->titre = $request->titre;
        $evenement->description = $request->description;
        $evenement->typeevenement_id=$request->typeevenement_id;
        $evenement->zone_id=$request->zone_id;
        $evenement->datedebut=$request->datedebut;
        $evenement->datefin=$request->datefin;
        $evenement->utilisateur_id=Auth::id();
        $evenement->save();
        Toastr::success('Evenement bien enregistrée.', 'Succès');
        return redirect()->route('dashboard.evenements');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //

        $evenement = Evenement::find($id);
        $zones = Zone::all()->sortBy('quartier_id');
        $arrondissements = Arrondissement::all();
        $types = Typeevenement::all();
        return view('dashboard/evenement/edit',compact('evenement','zones','types','arrondissements'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        //dd($request);
        if(Auth::user()->role_id!=2){
            $this->validate($request,[
                'titre' => 'required|max:100',
                'description' => 'required',
                'typeevenement_id' => 'required',
                'zone_id' => 'required',
                'datedebut' => 'required',
                'datefin' => 'required',
            ]);
        }

        $evenement = Evenement::find($id);
        if(Auth::user()->role_id==2 && $request->status!=""){
            $evenement->status=$request->status;
        }else{
            $evenement->titre = $request->titre;
            $evenement->description = $request->description;
            $evenement->typeevenement_id=$request->typeevenement_id;
            $evenement->zone_id=$request->zone_id;
            $evenement->datedebut=$request->datedebut;
            $evenement->datefin=$request->datefin;
        }
        $evenement->save();
        Toastr::success('Evenement bien enregistrée.', 'Succès');
        return redirect()->route('dashboard.evenements');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $evenement = Evenement::find($id);
        $evenement->forceDelete();

        Toastr::success('Evenement bien supprimée.', 'Succès');
        return redirect()->route('evenements');
    }
}
