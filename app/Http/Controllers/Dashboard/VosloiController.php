<?php
namespace App\Http\Controllers\Dashboard;

use App\Arrondissement;
use App\Loi;
use App\Typeloi;
use App\Zone;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;

class VosloiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $lois = Loi::where('utilisateur_id','=',Auth::id())->orderBy('id','desc')->get();
        return view('dashboard.voslois.index',compact('lois'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $zones = Zone::all()->sortBy('quartier_id');
        $arrondissements = Arrondissement::all();
        $types = Typeloi::all();
        return view('dashboard.voslois.create',compact('types','zones','arrondissements'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request,[
            'titre' => 'required|max:100',
            'description' => 'required',
            'typeloi_id' => 'required',
            'pdf' => 'required|mimes:pdf|max:15000'
        ]);

        $loi = new Loi();
        if($request->hasFile('pdf')){
            //$mediaUrl = Storage::putFile('publications',$request->file('media'),'public');
            $mediaUrl = $request->pdf;
            $mediaUrl_new_name = time().'.'.strtolower($mediaUrl->getClientOriginalExtension());
            $loi->pdfurl = 'lois/'.$mediaUrl_new_name;
            $mediaUrl->move('lois',$mediaUrl_new_name);
        }
        $loi->titre = $request->titre;
        $loi->description = $request->description;
        $loi->typeloi_id=$request->typeloi_id;
        $loi->zone_id=$request->zone_id;
        $loi->utilisateur_id=Auth::id();
        $loi->save();
        Toastr::success('Loi bien enregistrée.', 'Succès');
        return redirect()->route('dashboard.voslois.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $loi = Loi::find($id);
        $zones = Zone::all()->sortBy('quartier_id');
        $arrondissements = Arrondissement::all();
        $types = Typeloi::all();
        return view('dashboard.voslois.edit',compact('loi','zones','types','arrondissements'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $loi = Loi::find($id);
        $this->validate($request,[
            'titre' => 'required|max:100',
            'description' => 'required',
            'typeloi_id' => 'required',
            'pdf' => 'mimes:pdf|max:15000'
        ]);
        if($request->hasFile('pdf')){
            $mediaUrl = $request->pdf;
            $mediaUrl_new_name = time().'.'.strtolower($mediaUrl->getClientOriginalExtension());

            if(File::exists(public_path($loi->pdfurl))){
                unlink(public_path($loi->pdfurl));
            }
            $loi->pdfurl = 'lois/'.$mediaUrl_new_name;
            $mediaUrl->move('lois',$mediaUrl_new_name);
        }
        $loi->titre = $request->titre;
        $loi->description = $request->description;
        $loi->typeloi_id=$request->typeloi_id;
        $loi->zone_id=$request->zone_id;
        $loi->utilisateur_id=Auth::id();
        $loi->save();
        Toastr::success('Loi bien modifiée.', 'Succès');
        return redirect()->route('dashboard.voslois.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $loi = Loi::find($id);
        if(File::exists(public_path($loi->pdfurl))){
            unlink(public_path($loi->pdfurl));
        }

        $loi->forceDelete();

        Toastr::success('Loi bien supprimée.', 'Succès');
        return redirect()->route('dashboard.voslois.index');
    }
}
