<?php

namespace App\Http\Controllers\Dashboard;

use App\Typepublication;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TypesPublicationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $types = Typepublication::latest()->get();
        return view('dashboard.typepublication.index',compact('types'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('dashboard.typepublication.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request,[
            'name' => 'required'
        ]);
        $type = new Typepublication();
        $type->name = $request->name;
        $type->slug = str_slug($request->name);
        $type->save();
        Toastr::success('Type de publication bien enregistré', 'Succès');
        return redirect()->route('dashboard.typepublication.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $type = Typepublication::find($id);
        return view('dashboard.typepublication.edit', compact('type'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $this->validate($request,[
            'name' => 'required'
        ]);

        $type = Typepublication::find($id);
        $type->name = $request->name;
        $type->slug = str_slug($request->name);
        $type->save();

        Toastr::success('Type de publication bien modifié', 'Succès');
        return redirect()->route('dashboard.typepublication.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Typepublication::find($id)->delete();
        Toastr::success('Type de publication bien supprimé', 'Succès');
        return redirect()->back();
    }
}
