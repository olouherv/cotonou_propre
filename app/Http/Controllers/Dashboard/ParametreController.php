<?php

namespace App\Http\Controllers\dashboard;

use App\Utilisateur;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Foundation\Auth\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;

class ParametreController extends Controller
{
    //
    public function index(){
        return view('dashboard.parametres.index');
    }

    public function update(Request $request){
        if(Auth::user()->role_id != 3||Auth::user()->role_id != 2){
            $this->validate($request,[
                'name' => 'required',
            ]);
            if(Auth::user()->email!=$request->email){
                $this->validate($request,[
                    'email' => 'required|unique:utilisateurs|email',
                ]);
            }
            if(Auth::user()->username!=$request->username){
                $this->validate($request,[
                    'username' => 'required|unique:utilisateurs|min:5',
                ]);
            }
        }
        $this->validate($request,[
            'image' => 'mimes:jpeg,png,jpg',
            'telephone' => 'required|numeric|min:11',
            'about' => 'required|min:10',
        ]);

        $user = Utilisateur::find(Auth::id());
        $image = $request->file('image');
        if(isset($image)){
            $filename = time().'.'.strtolower($image->getClientOriginalExtension());

            if(isset($user->image) && File::exists(public_path($user->image))){
                unlink(public_path($user->image));
            }
            $image->move('profils',$filename);
            $filename = 'profils/'.$filename;
            $user->image = $filename;
        }

        if(Auth::user()->role_id != 3) {
            $user->name = $request->name;
            $user->email = $request->email;
        }

        $user->username = $request->username;
        $user->telephone = $request->telephone;
        $user->about = $request->about;
        $user->save();

        Toastr::success('Votre compte a été mise à jour.','Succès');
        return redirect()->back();

    }

    public function change(Request $request){
        $this->validate($request,[
            'mdpold' => 'required',
            'mdpnew' => 'required|min:6',
            'confirm_mdpnew' => 'required|min:6',
        ]);
        if($request->mdpnew == $request->confirm_mdpnew){
            if(Hash::check($request->mdpold,Auth::user()->password)){
                if(!Hash::check($request->mdpnew,Auth::user()->password)){
                    $user = Utilisateur::find(Auth::id());
                    $user->password = Hash::make($request->mdpnew);
                    $user->save();
                    Toastr::success('Votre mot de passe a été mis à jour.','Succès');
                    Auth::logout();
                    return redirect()->back();
                }else{
                    Toastr::error("Le nouveau mot de passe ne peut pas etre le meme que l'ancien",'Echec');
                    return redirect()->back();
                }
            }else{
                Toastr::error("L'ancien mot de passe n'est pas exact.",'Echec');
                return redirect()->back();
            }
        }else{
            Toastr::error("La confirmation et le nouveau mot de passe ne sont pas conformes.",'Echec');
            return redirect()->back();
        }

        Toastr::success('Votre mot de passe a été changé.','Succès');
        return redirect()->back();
    }
}
