<?php

namespace App\Http\Controllers\Dashboard;

use App\Arrondissement;
use App\Publication;
use App\Typepublication;
use App\Zone;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;

class PublicationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $publications = Publication::latest()->get();
        return view('dashboard.publication.index',compact('publications'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $zones = Zone::all()->sortBy('quartier_id');
        $arrondissements = Arrondissement::all();
        $types = Typepublication::all();
        return view('dashboard.publication.create',compact('types','zones','arrondissements'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request,[
            'titre' => 'required|max:100',
            'contenu' => 'required',
            'typePublication_id' => 'required',
            'media' => 'mimes:jpeg,jpg,png,mp4,avi,flv|max:15000'
        ]);

        $publication = new Publication();
        if($request->hasFile('media')){
            //$mediaUrl = Storage::putFile('publications',$request->file('media'),'public');
            $mediaUrl = $request->media;
            $mediaUrl_new_name = time().'.'.strtolower($mediaUrl->getClientOriginalExtension());
            $publication->mediaUrl = 'publications/'.$mediaUrl_new_name;
            //dd($request);
            $mediaUrl->move('publications',$mediaUrl_new_name);
        }

        $publication->titre = $request->titre;
        $publication->contenu = $request->contenu;
        $publication->typepublication_id=$request->typePublication_id;
        $publication->zone_id=$request->zone_id;
        $publication->utilisateur_id=Auth::id();
        $publication->slug = str_slug(time());
        $publication->save();
        Toastr::success('Publication bien enregistrée.', 'Succès');
        return redirect()->route('dashboard.publication.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Publication  $publication
     * @return \Illuminate\Http\Response
     */
    public function show(Publication $publication)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Publication  $publication
     * @return \Illuminate\Http\Response
     */
    public function edit(Publication $publication)
    {
        //
        $zones = Zone::all()->sortBy('quartier_id');
        $arrondissements = Arrondissement::all();
        $types = Typepublication::all();
        return view('dashboard.publication.edit',compact('publication','zones','types','arrondissements'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Publication  $publication
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Publication $publication)
    {
        //
        $this->validate($request,[
            'titre' => 'required|max:100',
            'contenu' => 'required',
            'typePublication_id' => 'required',
            'media' => 'mimes:jpeg,jpg,png,mp4,avi,flv|max:15000'
        ]);
        dd($request);
        if($request->hasFile('media')){
            $mediaUrl = $request->media;
            $mediaUrl_new_name = time().'.'.strtolower($mediaUrl->getClientOriginalExtension());

           if(File::exists(public_path($publication->mediaurl))){
               unlink(public_path($publication->mediaurl));
           }
           $publication->mediaurl = 'publications/'.$mediaUrl_new_name;
           $mediaUrl->move('publications',$mediaUrl_new_name);
        }
        $publication->titre = $request->titre;
        $publication->contenu = $request->contenu;
        $publication->typepublication_id=$request->typePublication_id;
        $publication->zone_id=$request->zone_id;
        $publication->utilisateur_id=Auth::id();
        //$publication->slug = str_slug(time());
        $publication->save();
        Toastr::success('Publication bien modifiée.', 'Succès');
        return redirect()->route('dashboard.publication.index');


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Publication  $publication
     * @return \Illuminate\Http\Response
     */
    public function destroy(Publication $publication)
    {
        //
        if(File::exists(public_path($publication->mediaurl))){
            unlink(public_path($publication->mediaurl));
        }

        $publication->forceDelete();

        Toastr::success('Publication bien supprimée.', 'Succès');
        return redirect()->route('dashboard.publication.index');
    }
}
