<?php

namespace App\Http\Controllers\Dashboard;

use App\Arrondissement;
use App\Utilisateur;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ArrondissementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        $arrondissements = Arrondissement::orderBy('name','asc')->get();
        return view('dashboard.arrondissement.index',compact('arrondissements'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $users = Utilisateur::all()->where('role_id','=','2');
        return view('dashboard.arrondissement.create',compact('users'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request,[
            'name' => 'required',
        ]);
        $arrondissement = new Arrondissement();
        $arrondissement->name = $request->name;
        $arrondissement->responsable_id = $request->responsable_id;
        $arrondissement->slug = str_slug($request->name);
        $arrondissement->save();
        Toastr::success('Arrondissement bien enregistré', 'Succès');
        return redirect()->route('dashboard.arrondissement.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $arrondissement = Arrondissement::find($id);
        $users = Utilisateur::all()->where('role_id','=','2');
        return view('dashboard.arrondissement.edit', compact('arrondissement','users'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $this->validate($request,[
            'name' => 'required',
        ]);

        $arrondissement = Arrondissement::find($id);
        $arrondissement->name = $request->name;
        $arrondissement->responsable_id = $request->responsable_id;
        $arrondissement->slug = str_slug($request->name);
        $arrondissement->save();

        Toastr::success('Arrondissement bien modifié', 'Succès');
        return redirect()->route('dashboard.arrondissement.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Arrondissement::find($id)->delete();
        Toastr::success('Arrondissement bien supprimé', 'Succès');
        return redirect()->back();
    }
}
