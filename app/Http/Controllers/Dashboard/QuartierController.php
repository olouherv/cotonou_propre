<?php

namespace App\Http\Controllers\Dashboard;

use App\Arrondissement;
use App\Quartier;
use App\Utilisateur;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class QuartierController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $quartiers = Quartier::orderBy('arrondissement_id','asc')->orderBy('name','asc')->get();
        return view('dashboard.quartier.index',compact('quartiers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $arrondissements = Arrondissement::all();
        $users = Utilisateur::all()->where('role_id','=','2');
        return view('dashboard.quartier.create',compact('users','arrondissements'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request,[
            'name' => 'required',
            'arrondissement_id' => 'required',
        ]);
        $quartier = new Quartier();
        $quartier->name = $request->name;
        $quartier->arrondissement_id = $request->arrondissement_id;
        $quartier->responsable_id = $request->responsable_id;
        $quartier->slug = str_slug($request->name);
        $quartier->save();
        Toastr::success('Quartier bien enregistré', 'Succès');
        return redirect()->route('dashboard.quartier.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $quartier = Quartier::find($id);
        $users = Utilisateur::all();
        $arrondissements = Arrondissement::all();
        return view('dashboard.quartier.edit', compact('quartier','users','arrondissements'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $this->validate($request,[
            'name' => 'required',
            'arrondissement_id' => 'required',
        ]);

        $quartier = Quartier::find($id);
        $quartier->name = $request->name;
        $quartier->arrondissement_id = $request->arrondissement_id;
        $quartier->responsable_id = $request->responsable_id;
        $quartier->slug = str_slug($request->name);
        $quartier->save();

        Toastr::success('Quartier bien modifié', 'Succès');
        return redirect()->route('dashboard.quartier.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Quartier::find($id)->delete();
        Toastr::success('Quartier bien supprimé', 'Succès');
        return redirect()->back();
    }
}
