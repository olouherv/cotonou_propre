<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class LikesController extends Controller
{
    //
    public function index(){
        $publications = Auth::user()->like_publications;
        return view('dashboard.favoris.index',compact('publications'));
    }
}
