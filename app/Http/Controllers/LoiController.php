<?php

namespace App\Http\Controllers;

use App\Arrondissement;
use App\Loi;
use App\Typeevenement;
use App\Typeloi;
use App\Typepublication;
use Illuminate\Http\Request;

class LoiController extends Controller
{
    //
    public function index($type=null){
        $types = Typeloi::orderBy('name','asc')->get();
        $arrondissements = Arrondissement::orderBy('id','asc')->get();
        if(isset($type)){
            $lois = Loi::whereHas('typeloi',function($query)use ($type){
                $query->where('slug',$type);
            })->orderBy('created_at','asc')->paginate(6);
        }else{
            $lois = Loi::orderBy('created_at','asc')->paginate(6);
        }
        return view('lois',compact('lois','types','arrondissements'));
    }

    public function arrondissement($slug)
    {
        $types = Typeloi::orderBy('name','asc')->get();
        $arrondissements = Arrondissement::orderBy('id','asc')->get();

        $lois = Loi::whereHas('zone.quartier.arrondissement', function ($query) use ($slug){
            $query->where('slug',$slug);
        })->orderBy('id','desc')->paginate(6);

        return view('lois',compact('lois','types','arrondissements'));
    }


}
