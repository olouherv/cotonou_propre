<?php

namespace App\Http\Controllers;

use App\Arrondissement;
use App\Evenement;
use App\Loi;
use App\Publication;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    /*public function __construct()
    {
        $this->middleware('auth');
    }*/

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $arrondissements = Arrondissement::orderBy('id','asc')->get();
        $pubs_rec = Publication::orderBy('id','desc')->take(4)->get();
        $lois_rec = Loi::orderBy('id','desc')->take(3)->get();
        $evts_rec = Evenement::where('status','1')->orderBy('id','desc')->take(3)->get();

        $pubs_vues = Publication::where('nbrevues','>','0')->orderBy('nbrevues','desc')->take(5)->get();
        $evts_next = Evenement::where('datedebut','>=',date("d/m/Y H:i"))
                                ->where('status','1')
                                ->orderBy('datedebut','asc')->take(5)->get();
        return view('accueil',compact('pubs_rec','lois_rec','evts_rec','evts_next','pubs_vues','arrondissements'));
    }

    public function newpub(){
        $pubs_rec = Publication::orderBy('id','desc')->take(4)->get();
        return view('ajax.newpub',compact('pubs_rec'));
    }

    public function newevents(){
        $evts_rec = Evenement::where('status','1')->orderBy('id','desc')->take(3)->get();
        return view('ajax.newevents',compact('evts_rec'));
    }

    public function newlois(){
        $lois_rec = Loi::orderBy('id','desc')->take(3)->get();
        return view('ajax.newlois',compact('lois_rec'));
    }

}
