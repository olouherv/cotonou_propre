<?php

namespace App\Http\Controllers;

use App\Utilisateur;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class InscriptionController extends Controller
{
    //
    public function inscription(Request $request)
    {
        if(Auth::id()!=null){
            return redirect()->back();
        }else {
            $this->validate($request, [
                'name' => 'required|string|max:255',
                'email' => 'required|string|email|max:255|unique:utilisateurs',
                'password' => 'required|string|min:6',
            ]);
            $utilisateur = new Utilisateur();
            $utilisateur->name = $request->name;
            $utilisateur->email = $request->email;
            $utilisateur->password = Hash::make($request->password);
            $utilisateur->username = str_slug($request->name);
            $utilisateur->save();
            Toastr::success("Veuillez vous connecter pour renseigner plus d'information!", 'Compte créé');
            return redirect()->back();
        }
    }
}
