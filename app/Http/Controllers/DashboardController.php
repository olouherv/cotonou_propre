<?php

namespace App\Http\Controllers;

use App\Evenement;
use App\Loi;
use App\Publication;
use App\Typeevenement;
use App\Typeloi;
use App\Typepublication;
use App\Utilisateur;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    //
    public function index(){
        $utilisateurs = Utilisateur::all();
        $publications = Publication::all();
        $lois = Loi::all();
        $typepublications = Typepublication::all();
        $typeevenements = Typeevenement::all();
        $typelois = Typeloi::all();
        $evenements = Evenement::all();
        return view('dashboard.index',compact('utilisateurs','publications','lois','evenements','typeevenements','typelois','typepublications'));
    }
}
