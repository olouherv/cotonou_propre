<?php

namespace App\Http\Controllers;

use App\Arrondissement;
use App\Publication;
use App\Typeevenement;
use App\Typepublication;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class PublicationsController extends Controller
{
    //

    public function add($id){
        $user = Auth::user();

        $isLike = $user->like_publications()->where('publication_id',$id)->count();

        if($isLike == 0){
            $user->like_publications()->attach($id);
            Toastr::success("Vous aimez cette publication.",'Succès');
            return redirect()->back();
        }else{
            $user->like_publications()->detach($id);
            Toastr::success("Vous n'aimez plus cette publication.",'Succès');
            return redirect()->back();
        }
    }

    public function details($slug){
        $typepublications = Typepublication::orderBy('name','asc')->get();
        $typeevenements = Typeevenement::orderBy('name','asc')->get();
        $arrondissements = Arrondissement::orderBy('id','asc')->get();

        $publication = Publication::where('slug',$slug)->first();
        $commentaires = $publication->commentaires()->get();
        $key = 'publication_'.$publication->id;
        if(!Session::has($key)){
            $publication->increment('nbrevues');
            Session::put($key,1);
        }
        return view('publication',compact('publication','typepublications','typeevenements','arrondissements','commentaires'));
    }

    public function index($type=null)
    {
        $typepublications = Typepublication::orderBy('name','asc')->get();
        $arrondissements = Arrondissement::orderBy('id','asc')->get();
        if(isset($type)){
            $publications = Publication::whereHas('typepublication', function ($query) use ($type){
                $query->where('slug',$type);
            })->orderBy('id','desc')->paginate(6);
        }else{
            $publications = Publication::latest()->paginate(6);
        }
        return view('publications',compact('publications','typepublications','arrondissements'));
    }

    public function arrondissement($slug)
    {
        $typepublications = Typepublication::orderBy('name','asc')->get();
        $arrondissements = Arrondissement::orderBy('id','asc')->get();

        $publications = Publication::whereHas('zone.quartier.arrondissement', function ($query) use ($slug){
            $query->where('slug',$slug);
        })->orderBy('id','desc')->paginate(6);

        return view('publications',compact('publications','typepublications','arrondissements'));
    }

}
