<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Evenement extends Model
{
    //
    public function utilisateur(){
        return $this->belongsTo('App\Utilisateur');
    }

    public function zone(){
        return $this->belongsTo('App\Zone');
    }

    public function typeevenement(){
        return $this->belongsTo('App\Typeevenement');
    }
}
