<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Utilisateur extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    public function role(){
        return $this->belongsTo('App\Role');
    }

    public function arrondissements(){
        return $this->hasMany('App\Arrondissement');
    }

    public function quartiers(){
        return $this->hasMany('App\Quartier');
    }

    public function zones(){
        return $this->hasMany('App\Zone');
    }

    public function publications(){
        return $this->hasMany('App\Publication');
    }

    public function evenements(){
        return $this->hasMany('App\Evenement');
    }

    public function lois(){
        return $this->hasMany('App\Loi');
    }

    public function like_publications(){
        return $this->belongsToMany('App\Publication')->withTimestamps();
    }

    public function commentaires(){
        return $this->hasMany('App\Commentaire');
    }
}
