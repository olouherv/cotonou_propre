<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Arrondissement extends Model
{
    //

    public function responsable(){
        return $this->belongsTo('App\Utilisateur');
    }

    public function quartiers(){
        return $this->hasMany('App\Quartier');
    }



}
