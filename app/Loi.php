<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Loi extends Model
{
    //
    public function zone(){
        return $this->belongsTo('App\Zone');
    }

    public function typeloi(){
       return $this->belongsTo('App\Typeloi');
    }

    public function utilisateur(){
        return $this->belongsTo('App\Utilisateur');
    }
}
