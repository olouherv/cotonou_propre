<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Publication extends Model
{
    //
    public function zone(){
        return $this->belongsTo('App\Zone');
    }

    public function utilisateur(){
        return $this->belongsTo('App\Utilisateur');
    }

    public function typepublication(){
        return $this->belongsTo('App\Typepublication');
    }

    public function commentaires(){
        return $this->hasMany('App\Commentaire');
    }

    public function like_utilisateurs(){
        return $this->belongsToMany('App\Utilisateur')->withTimestamps();
    }

}
