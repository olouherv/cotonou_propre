<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Zone extends Model
{
    //
    public function quartier(){
        return $this->belongsTo('App\Quartier');
    }

    public function responsable(){
        return $this->belongsTo('App\Utilisateur');
    }

    public function publications(){
        return $this->hasMany('App\Publication');
    }

    public function evenements(){
        return $this->hasMany('App\Evenement');
    }
}
