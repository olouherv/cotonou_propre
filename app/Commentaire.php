<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Commentaire extends Model
{
    //
    public function publication(){
        return $this->belongsTo('App\Publication');
    }

    public function utilisateur(){
        return $this->belongsTo('App\Utilisateur');
    }
}
