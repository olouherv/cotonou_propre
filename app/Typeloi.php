<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Typeloi extends Model
{
    //
    public function lois(){
        return $this->hasMany('App\Loi');
    }
}
