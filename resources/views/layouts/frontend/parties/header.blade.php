<header>
    @if(!Session::has('first'))
        <div class="container-fluid position-relative no-side-padding" style="text-align: center; margin: 20px;">
            <h1>Pour une capitale saine et propre, nous devons tous agir.</h1>
        </div>
        {{Session::put('first',1)}}
    @endif
    <div class="container-fluid position-relative no-side-padding">

        <a href="{{route('accueil')}}" class="logo"><img src="{{asset('assets/logo.png')}}" alt="Logo Image"></a>


        <div class="menu-nav-icon" data-nav-menu="#main-menu"><i class="ion-navicon"></i></div>

        <ul class="main-menu visible-on-click" id="main-menu">
            <li><a href="{{route('accueil')}}">Accueil</a></li>
            <li><a href="{{route('publications')}}">Publications</a></li>
            <li><a href="{{route('evenements')}}">Evenements</a></li>
            <li><a href="{{route('lois')}}">Lois</a></li>
            @if(!Auth::check())
                <li><a href="{{route('login')}}">Connexion</a></li>
                <li><a href="#inscription">S'inscrire</a></li>
            @else
                <li>
                    <a href="{{route('dashboard')}}">Espace Personnel</a>
                </li>

                <li>
                    <a class="dropdown-item" href="{{ route('logout') }}"
                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">

                        Déconnexion
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </li>
            @endif

        </ul><!-- main-menu -->

        <div class="src-area">
            <form>
                <button class="src-btn" type="submit"><i class="ion-ios-search-strong"></i></button>
                <input class="src-input" type="text" placeholder="Faire une recherche">
            </form>
        </div>

    </div><!-- conatiner -->
</header>
