@if(!Request::is('login') && !Request::is('register'))
    <footer>
        <div class="container">
            <div class="row">

                <div class="col-lg-9 col-md-6">
                    <div class="footer-section" style="text-align: center;">

                        <a class="logo" href="#"><img src="{{asset('assets/logo.png')}}" alt="Logo Image"></a>
                        <br><br>
                        <p class="copyright">Bona @ 2017. All rights reserved.</p>
                        <br>
                        <p class="copyright">Designed by <a href="https://colorlib.com" target="_blank">Colorlib</a></p>
                        <br>
                        <ul class="icons">
                            <li><a href="#"><i class="ion-social-facebook-outline"></i></a></li>
                            <li><a href="#"><i class="ion-social-twitter-outline"></i></a></li>
                            <li><a href="#"><i class="ion-social-instagram-outline"></i></a></li>
                            <li><a href="#"><i class="ion-social-googleplus-outline"></i></a></li>
                        </ul>

                    </div><!-- footer-section -->
                </div><!-- col-lg-3 col-md-6 -->

                <div class="col-lg-3 col-md-6">
                    <div class="footer-section">

                        <h4 class="title" id="inscription"><b>S'INSCRIRE</b></h4>
                        @guest
                            <form action="{{route('inscription')}}" method="post">
                                @csrf
                                <div class="input-area form-group">
                                    <input class="email-input" type="text" name="name" placeholder="Votre nom">
                                </div>
                                <div class="input-area form-group">
                                    <input class="email-input" type="text" name="email" placeholder="Votre email">
                                </div>
                                <div class="input-area form-group">
                                    <input class="email-input" type="password" name="password" placeholder="Votre mot de passe">
                                </div>
                                <button class="btn btn-block btn-primary" type="submit">ENVOYER</button>
                            </form>
                        @else
                            Vous etes déjà inscrit!
                        @endguest
                    </div><!-- footer-section -->
                </div><!-- col-lg-3 col-md-6 -->

            </div><!-- row -->
        </div><!-- container -->
    </footer>
@endif
