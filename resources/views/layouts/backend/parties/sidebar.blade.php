<aside id="leftsidebar" class="sidebar">
    <!-- User Info -->
    <div class="user-info">
        <div class="image">
            <img src="{{asset((File::exists(Auth::user()->image))?Auth::user()->image:'assets/backend/images/user.png')}}" width="48" height="48" alt="User" />
        </div>
        <div class="info-container">
            <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{Auth::user()->name}}</div>
            <div class="email">{{Auth::user()->email}}</div>
            {{--<div class="btn-group user-helper-dropdown">
                <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                <ul class="dropdown-menu pull-right">
                    <li><a href="javascript:void(0);"><i class="material-icons">person</i>Profil</a></li>
                    <li role="separator" class="divider"></li>

                </ul>
            </div>--}}
        </div>
    </div>
    <!-- #User Info -->
    <!-- Menu -->
    <div class="menu">
        <ul class="list">
            <li class="header">MAIN NAVIGATION</li>

            @if(Request::is('dashboard*'))
                <li class="{{Request::is('dashboard')? 'active' : ''}}">
                    <a href="{{route('dashboard')}}">
                        <i class="material-icons">dashboard</i>
                        <span>Tableau de bord</span>
                    </a>
                </li>
                @if(Auth::user()->role_id==1)
                    <li class="header">TYPES</li>
                    <li role="separator" class="divider"></li>
                    <li class="{{Request::is('dashboard/typeevenement*')? 'active' : ''}}">
                        <a href="{{route('dashboard.typeevenement.index')}}">
                            <i class="material-icons">label</i>
                            <span>Types d'évènement</span>
                        </a>
                    </li>
                    <li class="{{Request::is('dashboard/typepublication*')? 'active' : ''}}">
                        <a href="{{route('dashboard.typepublication.index')}}">
                            <i class="material-icons">label</i>
                            <span>Types de publication</span>
                        </a>
                    </li>
                    <li class="{{Request::is('dashboard/typeloi*')? 'active' : ''}}">
                        <a href="{{route('dashboard.typeloi.index')}}">
                            <i class="material-icons">label</i>
                            <span>Types de loi</span>
                        </a>
                    </li>
                    <li class="header">LIEUX</li>
                    <li role="separator" class="divider"></li>
                    <li class="{{Request::is('dashboard/arrondissement*')? 'active' : ''}}">
                            <a href="{{route('dashboard.arrondissement.index')}}">
                                <i class="material-icons">view_quilt</i>
                                <span>Arrondissements</span>
                            </a>
                        </li>
                    <li class="{{Request::is('dashboard/quartier*')? 'active' : ''}}">
                        <a href="{{route('dashboard.quartier.index')}}">
                            <i class="material-icons">view_list</i>
                            <span>Quartiers</span>
                        </a>
                    </li>
                    <li class="{{Request::is('dashboard/zone*')? 'active' : ''}}">
                        <a href="{{route('dashboard.zone.index')}}">
                            <i class="material-icons">view_module</i>
                            <span>Zones</span>
                        </a>
                    </li>
                @endif
                @if(Auth::user()->role_id != 4)
                    <li class="header">PUBLICATIONS</li>
                    <li role="separator" class="divider"></li>

                    @if(Auth::user()->role_id == 1)
                        <li class="{{Request::is('dashboard/publication*')? 'active' : ''}}">
                            <a href="{{route('dashboard.publication.index')}}">
                                <i class="material-icons">library_books</i>
                                <span>Publications</span>
                            </a>
                        </li>
                        <li class="{{Request::is('dashboard/commentaires*')? 'active' : ''}}">
                            <a href="{{route('commentaires')}}">
                                <i class="material-icons">comment</i>
                                <span>Commentaires</span>
                            </a>
                        </li>
                        <li class="{{Request::is('dashboard/loi*')? 'active' : ''}}">
                            <a href="{{route('dashboard.loi.index')}}">
                                <i class="material-icons">subject</i>
                                <span>Lois</span>
                            </a>
                        </li>
                        <li class="{{Request::is('dashboard/evenements*')? 'active' : ''}}">
                            <a href="{{route('dashboard.evenements')}}">
                                <i class="material-icons">event</i>
                                <span>Evenements</span>
                            </a>
                        </li>
                    @endif
                    @if(Auth::user()->role_id == 2)
                        <li class="{{Request::is('dashboard/evenements*')? 'active' : ''}}">
                            <a href="{{route('dashboard.evenements')}}">
                                <i class="material-icons">event</i>
                                <span>Evenements dans votre zone</span>
                            </a>
                        </li>
                    @endif
                    @if(Auth::user()->role_id == 3)
                        <li class="{{Request::is('dashboard/evenements*')? 'active' : ''}}">
                            <a href="{{route('dashboard.evenements')}}">
                                <i class="material-icons">event</i>
                                <span>Vos evenements</span>
                            </a>
                        </li>
                    @endif
                @endif
                <li class="header">PERSONNEL</li>
                <li role="separator" class="divider"></li>
                <li class="{{Request::is('dashboard/vospublications*')? 'active' : ''}}">
                    <a href="{{route('dashboard.vospublications')}}">
                        <i class="material-icons">library_books</i>
                        <span>Vos publications</span>
                    </a>
                </li>
                <li class="{{Request::is('dashboard/voscommentaires*')? 'active' : ''}}">
                    <a href="{{route('voscommentaires')}}">
                        <i class="material-icons">comment</i>
                        <span>Vos commentaires</span>
                    </a>
                </li>
                @if(Auth::user()->role_id == 1 || Auth::user()->role_id == 2 )
                    <li class="{{Request::is('dashboard/voslois*')? 'active' : ''}}">
                        <a href="{{route('dashboard.voslois.index')}}">
                            <i class="material-icons">subject</i>
                            <span>Vos lois</span>
                        </a>
                    </li>
                @endif
                <li class="{{Request::is('dashboard/favoris*')? 'active' : ''}}">
                    <a href="{{route('dashboard.favoris')}}">
                        <i class="material-icons">favorite</i>
                        <span>Vos favoris</span>
                    </a>
                </li>
                <li class="header">SYSTEME</li>
                <li role="separator" class="divider"></li>

                @if(Auth::user()->role_id==1)
                    <li class="{{Request::is('dashboard/utilisateurs*')? 'active' : ''}}">
                        <a href="{{route('dashboard.utilisateurs.index')}}">
                            <i class="material-icons">perm_identity</i>
                            <span>Utilisateurs</span>
                        </a>
                    </li>
                @endif

                <li class="{{Request::is('dashboard/parametres*')? 'active' : ''}}">
                    <a href="{{route('dashboard.parametres')}}">
                        <i class="material-icons">settings</i>
                        <span>Parametres</span>
                    </a>
                </li>
                <li>
                    <a href="{{route("accueil")}}">
                        <i class="material-icons">home</i>
                        <span>Retour à l'accueil</span>
                    </a>
                </li>
                <li>

                    <a class="dropdown-item" href="{{ route('logout') }}"
                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                        <i class="material-icons">input</i>
                        <span>Déconnexion</span>
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>

                </li>
            @endif

        </ul>
    </div>
    <!-- #Menu -->
    <!-- Footer -->
    <div class="legal">
        <div class="copyright">
            &copy; 2016 - 2017 <a href="javascript:void(0);">AdminBSB - Material Design</a>.
        </div>
        <div class="version">
            <b>Version: </b> 1.0.5
        </div>
    </div>
    <!-- #Footer -->
</aside>
