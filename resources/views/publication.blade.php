@extends('layouts.frontend.app')

@section('title')
    {{$publication->titre}}
@endsection

@push('css')
    <link href="{{asset('assets/frontend/css/publication/responsive.css')}}" rel="stylesheet">
    <link href="{{asset('assets/frontend/css/publication/styles.css')}}" rel="stylesheet">
    <style>
        .like_publications{
            color:green;
        }

        .header-bg{
            height: 400px;
            width: 100%;
            background-image: url("{{asset('assets/frontend/images/slider-1.jpg')}}");
        }

    </style>
@endpush

@section('content')

    <div class="header-bg">
        <div class="display-table  center-text">
            <h1 class="title display-table-cell"><b>PUBLICATION</b></h1>
        </div>
    </div>

    {{--<div class="slider">
        <div class="display-table  center-text">
            <h1 class="title display-table-cell"><b>PUBLICATION</b></h1>
        </div>
    </div><!-- slider -->--}}

    <section class="post-area section">
        <div class="container">

            <div class="row">

                <div class="col-lg-8 col-md-12 no-right-padding">

                    <div class="main-post">

                        <div class="blog-post-inner">

                            <div class="post-info">

                                <div class="left-area">
                                    <a class="avatar" href="#"><img src="{{asset($publication->utilisateur->image)}}" alt="Profile Image"></a>
                                </div>

                                <div class="middle-area">
                                    <a class="name" href="#"><b>{{$publication->utilisateur->name}}</b></a>
                                    <h6 class="date"> publié le : {{$publication->created_at}}</h6>
                                </div>

                            </div><!-- post-info -->

                            <h3 class="title"><a href="#"><b>{{$publication->titre}}</b></a></h3>

                            <p class="para">
                                {!!html_entity_decode($publication->contenu)!!}
                            </p>

                            <div class="post-image" ><img src="{{asset($publication->mediaurl)}}" alt="Blog Image"></div>

                        </div><!-- blog-post-inner -->

                        <div class="post-icons-area" style="color:inherit; background: #D2E2FC;">
                            <ul class="post-icons" style="padding-left: 10px;">
                                <li>

                                    @guest
                                        <a href="javascript:void(0)" onclick="toastr.info('Vous devez être connecté','Info',{
                                                closeButton: true,
                                                progressBar: true,
                                            })"><i class="ion-heart"></i>{{$publication->like_utilisateurs->count()}}</a>
                                    @else
                                        <a href="{{route('publications.like',$publication->id)}}"
                                           class="{{(!Auth::user()->like_publications->where('pivot.publication_id',$publication->id)->count()==0)?'like_publications':''}}">
                                            <i class="ion-heart"></i>{{$publication->like_utilisateurs->count()}}
                                        </a>
                                    @endguest

                                </li>
                                <li><a><i class="ion-chatbubble"></i>{{$publication->commentaires->count()}}</a></li>
                                <li><a><i class="ion-eye"></i>{{$publication->nbrevues}}</a></li>
                            </ul>

                            <ul class="icons">
                                <li>PARTAGER : </li>
                                <li><a href="https://www.facebook.com/sharer/sharer.php?u={{ URL::current() }}&display=popup"}}><i class="ion-social-facebook"></i></a></li>
                                <li><a href="https://twitter.com/intent/tweet?url={{ URL::current() }}"><i class="ion-social-twitter"></i></a></li>
                                <li><a href="https://plus.google.com/share?url={{ URL::current() }}"><i class="ion-social-googleplus"></i></a></li>
                            </ul>
                        </div>

                    </div><!-- main-post -->
                </div><!-- col-lg-8 col-md-12 -->

                <div class="col-lg-4 col-md-12 no-left-padding">

                    <div class="single-post info-area">

                        <div class="sidebar-area about-area">
                            <h4 class="title"><b>A PROPOS DE {{$publication->utilisateur->name}}</b></h4>
                            <p>{{$publication->utilisateur->about}}</p>
                        </div>

                        <div class="tag-area">

                            <h4 class="title"><b>DESCRIPTION</b></h4>
                            <ul>
                                <li><a href="{{route('publications.type',$publication->typepublication->slug)}}">{{$publication->typepublication->name}}</a></li>
                                <li><a href="javascript:void(0);">{{$publication->zone->name}}</a></li>
                                <li><a href="javascript:void(0);">{{$publication->zone->quartier->name}}</a></li>
                                <li><a href="{{route('publications.arrondissement',$publication->zone->quartier->arrondissement->slug)}}">{{$publication->zone->quartier->arrondissement->name}}</a></li>
                            </ul>

                        </div><!-- subscribe-area -->

                    </div><!-- info-area -->

                </div><!-- col-lg-4 col-md-12 -->

            </div><!-- row -->

        </div><!-- container -->
    </section><!-- post-area -->

    <section class="comment-section">
        <div class="container">
            <br>
            <h4><b>COMMENTAIRES</b></h4>
            <div class="row">

                <div class="col-lg-8 col-md-12">
                    <div class="comment-form">
                        @guest
                            <p>
                                Pour poster un commentaire, vous devez vous connecter.<a href="{{route('login')}}">Se connecter</a>
                            </p>
                        @else
                            <form method="post" action="{{route('commentaire.store',$publication->id)}}">
                                @csrf
                                <div class="row">

                                    <div class="col-sm-12">
									<textarea name="commentaire" rows="2" class="text-area-messge form-control"
                                              placeholder="Saisir votre commentaire" aria-required="true" aria-invalid="false"></textarea >
                                    </div><!-- col-sm-12 -->
                                    <div class="col-sm-12">
                                        <button class="submit-btn" type="submit" id="form-submit"><b>Envoyer</b></button>
                                    </div><!-- col-sm-12 -->

                                </div><!-- row -->
                            </form>
                        @endguest
                    </div><!-- comment-form -->

                    <h4><b>COMMENTAIRES({{$publication->commentaires->count()}})</b></h4>
                    @if(isset($commentaires))
                        <div class="commnets-area ">

                            @foreach($commentaires as $commentaire)

                                <div class="comment">

                                    <div class="post-info">

                                        <div class="left-area">
                                            <a class="avatar" href="#"><img src="{{asset($commentaire->utilisateur->image)}}" alt="Profile Image"></a>
                                        </div>

                                        <div class="middle-area">
                                            <a class="name" href="#"><b>{{$commentaire->utilisateur->name}}</b></a>
                                            <h6 class="date">commenté le {{$commentaire->created_at}}</h6>
                                        </div>

                                    </div><!-- post-info -->

                                    <p>{{$commentaire->commentaire}}</p>

                                </div>

                            @endforeach



                        </div><!-- commnets-area -->
                    @else
                        Aucun commentaire!!!
                    @endif
                </div><!-- col-lg-8 col-md-12 -->

            </div><!-- row -->

        </div><!-- container -->
    </section>
@endsection

@push('js')
    <script src="{{asset('assets/frontend/js/swiper.js')}}"></script>
@endpush
