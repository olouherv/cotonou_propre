@extends('layouts.frontend.app')

@section('title','Evenements')

@push('css')

    <link href="{{asset('assets/frontend/css/evenements/responsive.css')}}" rel="stylesheet">
    <link href="{{asset('assets/frontend/css/evenements/styles.css')}}" rel="stylesheet">
    <style>
        .like_publications{
            color:green;
        }

        {{--.header-bg{
            height: 400px;
            width: 100%;
            background-image: url("{{asset($publication->mediaurl)}}");
        }--}}

    </style>

@endpush

@section('content')
    <section class="blog-area section">
        <div class="container">

            <div class="row">
                <div class="col-md-9 col-lg-9 pull-left">
                    @foreach($evenements as $evenement)
                        <div style="margin-bottom: 30px">
                            <div class="card h-100">
                                <div class="single-post post-style-2">
                                    <div style="padding: 30px;">
                                        <h6 class="pre-title"><a href="#"><b>{{$evenement->typeevenement->name}}</b></a></h6>

                                        <h4 class="title"><a href="#"><b>{{$evenement->titre}}</b> - {{$evenement->zone->name}}\{{$evenement->zone->quartier->name}}\{{$evenement->zone->quartier->arrondissement->name}}</a></h4>

                                        <p>{{$evenement->description}}</p>

                                        <div class="avatar-area">
                                            <a class="avatar" href="#"><img src="{{asset($evenement->utilisateur->image)}}" alt="Profile Image"></a>
                                            <div class="right-area">
                                                <a class="name" href="#"><b>{{$evenement->utilisateur->name}}</b></a>
                                                <h6 class="date" href="#">publié le : {{$evenement->created_at}}</h6>
                                            </div>
                                        </div>

                                    </div>
                                    <ul class="post-footer">
                                        <li><a><i class="ion-calendar"></i>{{$evenement->datedebut}}</a></li>
                                        <li><a><i class="ion-calendar"></i>{{$evenement->datefin}}</a></li>
                                    </ul>
                                </div><!-- single-post extra-blog -->
                            </div><!-- card -->
                        </div><!-- col-lg-12 col-md-12 -->
                    @endforeach
                </div>
                <div class="col-md-3 col-lg-3 pull-right" style="text-align: left; background: white;">
                    <div>
                        <h4 style="margin-bottom: 10px;"><b>Type d'évènements</b></h4>
                        <hr>
                        <ol class="list-unstyled">
                            <li style="margin: 5px 0 5px 0;"><a class="btn btn-default" href="{{route('evenements')}}" style="background: whitesmoke;">TOUT</a></li>
                            @foreach($typeevenements as $type)
                                <li style="margin: 5px 0 5px 0;"><a class="btn btn-default" href="{{route('evenements.type',$type->slug)}}" style="background: whitesmoke;">{{$type->name}}</a></li>
                            @endforeach
                        </ol>
                        <hr>
                        <h4 style="margin-bottom: 10px;"><b>Arrondissements</b></h4>
                        <hr>
                        <ol class="list-unstyled">
                            <li style="margin: 5px 0 5px 0;"><a class="btn btn-default" href="{{route('evenements')}}" style="background: whitesmoke;">TOUT</a></li>
                            @foreach($arrondissements as $arrondissement)
                                <li style="margin: 5px 0 5px 0;"><a class="btn btn-default" href="{{route('evenements.arrondissement',$arrondissement->slug)}}" style="background: whitesmoke;">{{$arrondissement->name}}</a></li>
                            @endforeach
                        </ol>
                        <hr>
                    </div>
                </div>
            </div><!-- row -->
            {{$evenements->links()}}

        </div><!-- container -->
    </section>
@endsection

@push('js')
    <script src="{{asset('assets/frontend/js/swiper.js')}}"></script>
@endpush
