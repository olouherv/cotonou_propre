<div class="row">
    @if($pubs_rec->count()>0)
        @foreach($pubs_rec as $publication)

            <div class="col-lg-6 col-md-6">
                <div class="card h-100">
                    <div class="single-post post-style-1">

                        <div class="blog-image">
                            @if(str_contains($publication->mediaurl,['jpeg','jpg','png']))
                                <img src="{{asset($publication->mediaurl)}}" alt="Blog Image">
                            @elseif(str_contains($publication->mediaurl,['avi','mp4','flv']))
                                <video src="{{asset($publication->mediaurl)}}" controls/>
                            @endif
                        </div>

                        <a class="avatar" href="#"><img src="{{asset($publication->utilisateur->image)}}" alt="Profile Image"></a>
                        <div class="blog-info">

                            <h4 class="title"><a href="{{route('publications.details',$publication->slug)}}"><b>{{$publication->titre}}</b></a></h4>

                            <ul class="post-footer">
                                <li>

                                    @guest
                                        <a href="javascript:void(0)" onclick="toastr.info('Vous devez être connecté','Info',{
                                                        closeButton: true,
                                                        progressBar: true,
                                                    })"><i class="ion-heart"></i>{{$publication->like_utilisateurs->count()}}</a>
                                    @else
                                        <a href="{{route('publications.like',$publication->id)}}"
                                           class="{{(!Auth::user()->like_publications->where('pivot.publication_id',$publication->id)->count()==0)?'like_publications':''}}">
                                            <i class="ion-heart"></i>{{$publication->like_utilisateurs->count()}}
                                        </a>
                                    @endguest

                                </li>
                                <li><a><i class="ion-chatbubble"></i>{{$publication->commentaires->count()}}</a></li>
                                <li><a><i class="ion-eye"></i>{{$publication->nbrevues}}</a></li>
                            </ul>

                        </div><!-- blog-info -->
                    </div><!-- single-post -->
                </div><!-- card -->
            </div><!-- col-lg-4 col-md-6 -->

        @endforeach
    @else
        Aucune publication
    @endif
</div>
@if($pubs_rec->count()==4)
    <a class="load-more-btn" href="{{route('publications')}}"><b>VOIR PLUS</b></a>
@endif
