<div class="row">
    @if($lois_rec->count()>0)
        @foreach($lois_rec as $loi)
            <div class="col-lg-12 col-md-12">
                <div class="card h-100">
                    <div class="single-post post-style-2">
                        <div style="padding: 30px;">
                            <h6 class="pre-title"><a href="#"><b>{{$loi->typeloi->name}}</b></a></h6>

                            <h4 class="title"><a href="#"><b>{{$loi->titre}}</b> -
                                    @if($loi->zone!=0)
                                        {{$loi->zone->name}}\{{$loi->zone->quartier->name}}\{{$loi->zone->quartier->arrondissement->name}}</a></h4>
                            @else
                                Toutes les zones
                            @endif
                            <p>{{$loi->description}}</p>

                            <div class="avatar-area">
                                <a class="avatar" href="#"><img src="{{asset($loi->utilisateur->image)}}" alt="Profile Image"></a>
                                <div class="right-area">
                                    <a class="name" href="#"><b>{{$loi->utilisateur->name}}</b></a>
                                    <h6 class="date" href="#">publié le : {{$loi->created_at}}</h6>
                                </div>
                            </div>

                        </div>
                        <ul class="post-footer">
                            <li>
                                @guest
                                    <a href="javascript:void(0)" onclick="toastr.info('Vous devez être connecté','Info',{
                                                closeButton: true,
                                                progressBar: true,
                                            })"><i class="ion-document"></i>Telecharger le document</a>
                                @else
                                    <a href="{{asset($loi->pdfurl)}}"><i class="ion-document">Telecharger le document</i></a>

                            @endguest
                        </ul>
                    </div><!-- single-post extra-blog -->

                </div><!-- card -->
            </div><!-- col-lg-12 col-md-12 -->
        @endforeach
    @else
        Aucune loi
    @endif
</div>
@if($lois_rec->count()==3)
    <a class="load-more-btn" href="{{route('lois')}}"><b>VOIR PLUS</b></a>
@endif
