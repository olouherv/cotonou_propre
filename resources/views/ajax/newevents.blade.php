<div class="row">
    @if($evts_rec->count()>0)
        @foreach($evts_rec as $evenement)
            <div class="col-lg-12 col-md-12">
                <div class="card h-100">
                    <div class="single-post post-style-2">
                        <div style="padding: 30px;">
                            <h6 class="pre-title"><a href="#"><b>{{$evenement->typeevenement->name}}</b></a></h6>

                            <h4 class="title"><a href="#"><b>{{$evenement->titre}}</b> - {{$evenement->zone->name}}\{{$evenement->zone->quartier->name}}\{{$evenement->zone->quartier->arrondissement->name}}</a></h4>

                            <p>{{$evenement->description}}</p>

                            <div class="avatar-area">
                                <a class="avatar" href="#"><img src="{{asset($evenement->utilisateur->image)}}" alt="Profile Image"></a>
                                <div class="right-area">
                                    <a class="name" href="#"><b>{{$evenement->utilisateur->name}}</b></a>
                                    <h6 class="date" href="#">publié le : {{$evenement->created_at}}</h6>
                                </div>
                            </div>

                        </div>
                        <ul class="post-footer">
                            <li style="width: 50%;"><a><i class="ion-calendar"></i>{{$evenement->datedebut}}</a></li>
                            <li style="width: 50%;"><a><i class="ion-calendar"></i>{{$evenement->datefin}}</a></li>
                        </ul>
                    </div><!-- single-post extra-blog -->
                </div><!-- card -->
            </div><!-- col-lg-12 col-md-12 -->
        @endforeach
    @else
        Aucun évènement
    @endif
</div>
@if($evts_rec->count()==3)
    <a class="load-more-btn" href="{{route('evenements')}}"><b>VOIR PLUS</b></a>
@endif
