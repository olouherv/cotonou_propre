@extends('layouts.backend.app')

@section('title','Modifier Quartier')

@push('css')
    <!-- JQuery DataTable Css -->
    <link href="{{asset('assets/backend/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css')}}" rel="stylesheet">
    <!-- Bootstrap Select Css -->
    <link href="{{asset('assets/backend/plugins/bootstrap-select/css/bootstrap-select.css')}}" rel="stylesheet" />
@endpush


@section('content')
    <div class="container-fluid">
        <!-- Vertical Layout -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            MODIFIER QUARTIER
                        </h2>
                    </div>
                    <div class="body">
                        <form action="{{route('dashboard.quartier.update',$quartier->id)}}" method="POST">
                            @csrf
                            @method('put')
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" id="name" value="{{$quartier->name}}" class="form-control" name="name">
                                    <label class="form-label">Nom du quartier</label>
                                </div>
                            </div>

                            <div class="form-group bootstrap-select form-control show-tick">
                                <select class="form-control show-tick" name="arrondissement_id">
                                    <option value="0">Choisir l'Arrondissement</option>
                                    @foreach($arrondissements as $arrondissement)
                                        @if($arrondissement->id == $quartier->arrondissement_id)
                                            <option value="{{$arrondissement->id}}" selected>{{$arrondissement->name}}</option>
                                        @else
                                            <option value="{{$arrondissement->id}}">{{$arrondissement->name}}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                            <br>

                            <div class="form-group bootstrap-select form-control show-tick">
                                <select class="form-control show-tick" name="responsable_id">
                                    <option value="">Choisir Responsable</option>
                                    @foreach($users as $user)
                                        @if($user->id == $quartier->responsable_id)
                                            <option value="{{$user->id}}" selected>{{$user->name}}</option>
                                        @else
                                            <option value="{{$user->id}}">{{$user->name}}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>

                            <a class="btn btn-danger m-t-15 waves-effect" href="{{route('dashboard.quartier.index')}}">RETOUR</a>
                            <button type="submit" class="btn btn-primary m-t-15 waves-effect">ENREGISTRER</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Vertical Layout -->
    </div>


@endsection


@push('js')
    <!-- Jquery DataTable Plugin Js -->
    <script src="{{asset('assets/backend/plugins/jquery-datatable/jquery.dataTables.js')}}"></script>
    <script src="{{asset('assets/backend/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js')}}"></script>
    <script src="{{asset('assets/backend/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('assets/backend/plugins/jquery-datatable/extensions/export/buttons.flash.min.js')}}"></script>
    <script src="{{asset('assets/backend/plugins/jquery-datatable/extensions/export/jszip.min.js')}}"></script>
    <script src="{{asset('assets/backend/plugins/jquery-datatable/extensions/export/pdfmake.min.js')}}"></script>
    <script src="{{asset('assets/backend/plugins/jquery-datatable/extensions/export/vfs_fonts.js')}}"></script>
    <script src="{{asset('assets/backend/plugins/jquery-datatable/extensions/export/buttons.html5.min.js')}}"></script>
    <script src="{{asset('assets/backend/plugins/jquery-datatable/extensions/export/buttons.print.min.js')}}"></script>
    <script src="{{asset('assets/backend/js/pages/tables/jquery-datatable.js')}}"></script>
    <!-- Select Plugin Js -->
    <script src="{{asset('assets/backend/plugins/bootstrap-select/js/bootstrap-select.js')}}"></script>
@endpush
