@extends('layouts.backend.app')

@section('title','Quartiers')

@push('css')
    <!-- JQuery DataTable Css -->
    <link href="{{asset('assets/backend/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css')}}" rel="stylesheet">
@endpush


@section('content')
    <div class="container-fluid">
            <div class="block-header">
                <a class="btn btn-primary waves-effect" href="{{route('dashboard.quartier.create')}}">
                    <i class="material-icons">add</i>
                    <span>Nouveau Quartier</span>
                </a>
            </div>
            <!-- Exportable Table -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                TOUS LES QUARTIERS
                            </h2>

                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>NOM</th>
                                        <th>ARRONDISSEMENT</th>
                                        <th>RESPONSABLE</th>
                                        <th>ACTION</th>
                                    </tr>
                                    </thead>
                                    <tfoot>
                                    <tr>
                                        <th>ID</th>
                                        <th>NOM</th>
                                        <th>ARRONDISSEMENT</th>
                                        <th>RESPONSABLE</th>
                                        <th>ACTION</th>
                                    </tr>
                                    </tfoot>
                                    <tbody>

                                        @foreach($quartiers as $key=>$quartier)
                                            <tr>
                                                <td>{{$key + 1}}</td>
                                                <td>{{$quartier->name}}</td>
                                                <td>{{$quartier->arrondissement->name}}</td>
                                                <td>
                                                    @if($quartier->responsable_id != 0)
                                                        {{$quartier->responsable->name}}
                                                    @else
                                                        Pas de responsable
                                                    @endif
                                                </td>
                                                <td class="text-center">
                                                    <a class="btn btn-info waves-effect" href="{{route('dashboard.quartier.edit',$quartier->id)}}">
                                                        <i class="material-icons">edit</i>

                                                    </a>

                                                    <button class="btn btn-danger waves-effect"
                                                            onclick="deleteQuartier({{$quartier->id}})"
                                                            type="button">
                                                        <i class="material-icons">delete</i>
                                                    </button>

                                                    <form method="post" id="delete-form-{{$quartier->id}}" action="{{route('dashboard.quartier.destroy',$quartier->id)}}" style="display: none;">
                                                        @csrf
                                                        @method('DELETE')
                                                    </form>

                                                </td>
                                            </tr>
                                        @endforeach

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Exportable Table -->
        </div>
@endsection


@push('js')
    <!-- Jquery DataTable Plugin Js -->
    <script src="{{asset('assets/backend/plugins/jquery-datatable/jquery.dataTables.js')}}"></script>
    <script src="{{asset('assets/backend/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js')}}"></script>
    <script src="{{asset('assets/backend/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('assets/backend/plugins/jquery-datatable/extensions/export/buttons.flash.min.js')}}"></script>
    <script src="{{asset('assets/backend/plugins/jquery-datatable/extensions/export/jszip.min.js')}}"></script>
    <script src="{{asset('assets/backend/plugins/jquery-datatable/extensions/export/pdfmake.min.js')}}"></script>
    <script src="{{asset('assets/backend/plugins/jquery-datatable/extensions/export/vfs_fonts.js')}}"></script>
    <script src="{{asset('assets/backend/plugins/jquery-datatable/extensions/export/buttons.html5.min.js')}}"></script>
    <script src="{{asset('assets/backend/plugins/jquery-datatable/extensions/export/buttons.print.min.js')}}"></script>
    <script src="{{asset('assets/backend/js/pages/tables/jquery-datatable.js')}}"></script>

    <script src="https://unpkg.com/sweetalert@2.1.0/dist/sweetalert.min.js"></script>

    <script type="text/javascript">
        function deleteQuartier(id) {
            swal({
                title: "Are you sure?",
                text: "Once deleted, you will not be able to recover this imaginary file!",
                icon: "warning",
                confirmButtonText: 'Oui, supprimer ce type!',
                cancelButtonText: 'Non, annuler!',
                cancelButtonClass: 'btn btn-danger',
                confirmButtonClass:'btn btn-success',
                buttons: true,
                dangerMode: true,
            })
                .then((willDelete) => {
                    if (willDelete) {
                        event.preventDefault();
                        document.getElementById('delete-form-'+id).submit();
                        swal("Poof! Your imaginary file has been deleted!", {
                            icon: "success",
                        });
                    } else {
                        swal('Cancelled',
                            "Your imaginary file is safe!",
                            'error');
                    }
                });
        }

    </script>

@endpush
