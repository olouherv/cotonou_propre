@extends('layouts.backend.app')

@section('title','Nouvelle Zone')

@push('css')
    <!-- JQuery DataTable Css -->
    <link href="{{asset('assets/backend/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css')}}" rel="stylesheet">
    <!-- Bootstrap Select Css -->
    <link href="{{asset('assets/backend/plugins/bootstrap-select/css/bootstrap-select.css')}}" rel="stylesheet" />

@endpush


@section('content')
    <div class="container-fluid">
        <!-- Vertical Layout -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            AJOUTER UNE ZONE
                        </h2>
                    </div>
                    <div class="body">
                        <form action="{{route('dashboard.zone.store')}}" method="POST">
                            @csrf

                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" id="name" class="form-control" name="name">
                                    <label class="form-label">Nom d'une Zone</label>
                                </div>
                            </div>

                            <div class="form-group bootstrap-select form-control show-tick">
                                <select class="form-control show-tick" name="quartier_id">
                                    <option value="0">Choisir le quartier</option>
                                    @foreach($arrondissements as $arrondissement)
                                        <optgroup label="{{$arrondissement->name}}">

                                            @foreach($quartiers as $quartier)
                                                @if($arrondissement->name == $quartier->arrondissement->name)
                                                    <option value="{{$quartier->id}}">{{$quartier->name}}</option>
                                                @endif
                                            @endforeach

                                        </optgroup>
                                    @endforeach
                                </select>
                            </div>
                            <br>

                            <div class="form-group bootstrap-select form-control show-tick">
                                <select class="form-control show-tick" name="responsable_id">
                                    <option value="0">Choisir Responsable</option>
                                    @foreach($users as $user)
                                        <option value="{{$user->id}}">{{$user->name}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <a class="btn btn-danger m-t-15 waves-effect" href="{{route('dashboard.zone.index')}}">RETOUR</a>
                            <button type="submit" class="btn btn-primary m-t-15 waves-effect">ENREGISTRER</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Vertical Layout -->
    </div>


@endsection


@push('js')
    <!-- Jquery DataTable Plugin Js -->
    <script src="{{asset('assets/backend/plugins/jquery-datatable/jquery.dataTables.js')}}"></script>
    <script src="{{asset('assets/backend/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js')}}"></script>
    <script src="{{asset('assets/backend/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('assets/backend/plugins/jquery-datatable/extensions/export/buttons.flash.min.js')}}"></script>
    <script src="{{asset('assets/backend/plugins/jquery-datatable/extensions/export/jszip.min.js')}}"></script>
    <script src="{{asset('assets/backend/plugins/jquery-datatable/extensions/export/pdfmake.min.js')}}"></script>
    <script src="{{asset('assets/backend/plugins/jquery-datatable/extensions/export/vfs_fonts.js')}}"></script>
    <script src="{{asset('assets/backend/plugins/jquery-datatable/extensions/export/buttons.html5.min.js')}}"></script>
    <script src="{{asset('assets/backend/plugins/jquery-datatable/extensions/export/buttons.print.min.js')}}"></script>
    <script src="{{asset('assets/backend/js/pages/tables/jquery-datatable.js')}}"></script>
    <!-- Select Plugin Js -->
    <script src="{{asset('assets/backend/plugins/bootstrap-select/js/bootstrap-select.js')}}"></script>
@endpush
