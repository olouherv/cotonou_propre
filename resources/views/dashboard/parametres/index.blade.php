@extends('layouts.backend.app')

@section('title','Parametres')

@push('css')

@endpush


@section('content')
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            PARAMETRES
                        </h2>
                        <ul class="header-dropdown m-r--5">
                            <li class="dropdown">
                                <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                    <i class="material-icons">more_vert</i>
                                </a>
                                <ul class="dropdown-menu pull-right">
                                    <li><a href="javascript:void(0);" class=" waves-effect waves-block">Action</a></li>
                                    <li><a href="javascript:void(0);" class=" waves-effect waves-block">Another action</a></li>
                                    <li><a href="javascript:void(0);" class=" waves-effect waves-block">Something else here</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <div class="body">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs tab-nav-right" role="tablist">
                            <li role="presentation" class="active">
                                <a href="#profil" data-toggle="tab">
                                    <i class="material-icons">face</i>Modifier votre profil
                                </a>
                            </li>
                            <li role="presentation">
                                <a href="#motdepasse" data-toggle="tab">
                                    <i class="material-icons">change_history</i>Changer de mot de passe
                                </a>
                            </li>
                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane fade in active" id="profil">
                                <form class="form-horizontal" method="post" action="{{route('dashboard.parametres.update')}}" enctype="multipart/form-data">
                                    @csrf
                                    @method('PUT')

                                    <div class="row clearfix">
                                        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                            <label for="name">Votre nom</label>
                                        </div>
                                        <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <input type="text" id="name" class="form-control" name="name" value ="{{Auth::user()->name}}" {{(Auth::user()->role_id==3||Auth::user()->role_id==2)?'disabled':''}} placeholder="Entrer votre nom complet">
                                                </div>
                                            </div>
                                        </div>
                                    </div>{{--name--}}
                                    <div class="row clearfix">
                                        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                            <label for="username">Nom d'utilisateur</label>
                                        </div>
                                        <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <input type="text" id="username" class="form-control" name="username" value="{{Auth::user()->username}}" {{(Auth::user()->role_id==3||Auth::user()->role_id==2)?'disabled':''}} placeholder="Entrer votre nom d'utilisateur">
                                                </div>
                                            </div>
                                        </div>
                                    </div>{{--username--}}
                                    <div class="row clearfix">
                                        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                            <label for="email">Adresse Email</label>
                                        </div>
                                        <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <input type="text" id="email" class="form-control" name="email" value="{{Auth::user()->email}}" {{(Auth::user()->role_id==3||Auth::user()->role_id==2)?'disabled':''}} placeholder="Entrer votre adresse email">
                                                </div>
                                            </div>
                                        </div>
                                    </div>{{--email--}}
                                    <div class="row clearfix">
                                        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                            <label for="image">Photo de profil</label>
                                        </div>
                                        <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <input type="file" id="image" class="form-control" name="image">
                                                </div>
                                            </div>
                                        </div>
                                    </div>{{--Profil--}}
                                    <div class="row clearfix">
                                        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                            <label for="telephone">Telephone</label>
                                        </div>
                                        <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <input type="tel" id="telephone" class="form-control" name="telephone" value="{{Auth::user()->telephone}}" placeholder="Entrer votre numero de telephone">
                                                </div>
                                            </div>
                                        </div>
                                    </div>{{--Telephone--}}
                                    @if(Auth::user()->role_id == 3)
                                        <div class="row clearfix">
                                        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                            <label for="responsable">Responsable</label>
                                        </div>
                                        <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <input type="text" id="responsable" class="form-control" name="responsable" value="{{Auth::user()->responsable}}" disabled placeholder="Responsable de l'association">
                                                </div>
                                            </div>
                                        </div>
                                    </div>{{--Responsable--}}
                                    @endif
                                    <div class="row clearfix">
                                        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                            <label for="about">A propos de vous</label>
                                        </div>
                                        <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <textarea rows="5" id="about" class="form-control" name="about" placeholder="Dites plus sur vous">{{Auth::user()->about}}</textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>{{--About--}}
                                    <div class="row clearfix">
                                        <div class="col-lg-offset-2 col-md-offset-2 col-sm-offset-4 col-xs-offset-5">
                                            <button type="submit" class="btn btn-primary m-t-15 waves-effect">Mise à jour</button>
                                        </div>
                                    </div>{{--Maj--}}
                                </form>

                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="motdepasse">
                                <form class="form-horizontal" action="{{route('dashboard.parametres.change')}}" method="POST">
                                    @csrf
                                    @method('PUT')
                                    <div class="row clearfix">
                                        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                            <label for="mdpold">Mot de passe actuel</label>
                                        </div>
                                        <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <input type="password" id="mdpold" class="form-control" name="mdpold" placeholder="Mot de passe actuel">
                                                </div>
                                            </div>
                                        </div>
                                    </div>{{--mdpold--}}
                                    <div class="row clearfix">
                                        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                            <label for="mdpnew">Nouveau mot de passe</label>
                                        </div>
                                        <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <input type="password" id="mdpnew" class="form-control" name="mdpnew" placeholder="Nouveau mot de passe">
                                                </div>
                                            </div>
                                        </div>
                                    </div>{{--mdpnew--}}
                                    <div class="row clearfix">
                                        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                            <label for="confirm_mdpnew">Confirmation</label>
                                        </div>
                                        <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <input type="password" id="confirm_mdpnew" class="form-control" name="confirm_mdpnew" placeholder="Confirmation du nouveau mot de passe">
                                                </div>
                                            </div>
                                        </div>
                                    </div>{{--confirm--}}
                                    <div class="row clearfix">
                                        <div class="col-lg-offset-2 col-md-offset-2 col-sm-offset-4 col-xs-offset-5">
                                            <button type="submit" class="btn btn-primary m-t-15 waves-effect">Mise à jour</button>
                                        </div>
                                    </div>{{--Maj--}}
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@push('js')

@endpush
