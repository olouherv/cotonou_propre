@extends('layouts.backend.app')

@section('title','Modifier la publication')

@push('css')

    <!-- Bootstrap Select Css -->
    <link href="{{asset('assets/backend/plugins/bootstrap-select/css/bootstrap-select.css')}}" rel="stylesheet" />

@endpush


@section('content')
    <div class="container-fluid">
        <form action="{{route('dashboard.vospublications.update',$publication->id)}}" method="POST" enctype="multipart/form-data">
        @csrf
       {{--@method('PUT')--}}
        <!-- Vertical Layout -->
            <div class="row clearfix">
                <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                MODIFIER LA PUBLICATION
                            </h2>
                        </div>
                        <div class="body">

                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" id="name" class="form-control" name="titre" value="{{$publication->titre}}">
                                    <label class="form-label">Titre de la publication</label>
                                </div>
                            </div>

                            <span>Contenu de votre publication</span>
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <textarea type="text" id="tinymce" class="form-control" name="contenu">{{$publication->contenu}}</textarea>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                INFORMATION
                            </h2>
                        </div>
                        <div class="body">
                            <div class="form-group bootstrap-select form-control show-tick">
                                <select class="form-control show-tick" name="typePublication_id">
                                    <option value="">Choisir le type de publication</option>
                                    @foreach($types as $type)
                                        @if($publication->typepublication_id==$type->id)
                                            <option value="{{$type->id}}" selected>
                                                {{$type->name}}
                                            </option>
                                        @else
                                            <option value="{{$type->id}}">
                                                {{$type->name}}
                                            </option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                            <br>
                            <div class="form-group bootstrap-select form-control show-tick">
                                <select class="form-control show-tick" name="zone_id">
                                    <option value="">Choisir la zone</option>
                                    @foreach($arrondissements as $arrondissement)
                                        <optgroup label="{{$arrondissement->name}}">
                                            @foreach($zones as $zone)
                                                @if($zone->quartier->arrondissement->name == $arrondissement->name)
                                                    @if($publication->zone_id==$zone->id)
                                                        <option value="{{$zone->id}}" selected>
                                                            {{$zone->name}}\{{$zone->quartier->name}}
                                                        </option>
                                                    @else
                                                        <option value="{{$zone->id}}">
                                                            {{$zone->name}}\{{$zone->quartier->name}}
                                                        </option>
                                                    @endif
                                                @endif
                                            @endforeach
                                        </optgroup>
                                    @endforeach
                                </select>
                            </div>
                            <br>
                            <div class="form-group">
                                <label for="media">Joindre un fichier</label>
                                <input type="file" name="media">
                            </div>

                            <a class="btn btn-danger m-t-15 waves-effect" href="{{route('dashboard.vospublications')}}">RETOUR</a>
                            <button type="submit" class="btn btn-primary m-t-15 waves-effect">ENREGISTRER</button>
                        </div>
                    </div>
                </div>
            </div>

        </form>
    </div>


@endsection


@push('js')
    <!-- Jquery DataTable Plugin Js -->
    <script src="{{asset('assets/backend/plugins/tinymce/tinymce.js')}}"></script>
    <!-- Select Plugin Js -->
    <script src="{{asset('assets/backend/plugins/bootstrap-select/js/bootstrap-select.js')}}"></script>

    <script>
        $(function () {
            //TinyMCE
            tinymce.init({
                selector: "textarea#tinymce",
                theme: "modern",
                height: 300,
                plugins: [
                    'advlist autolink lists link image charmap print preview hr anchor pagebreak',
                    'searchreplace wordcount visualblocks visualchars code fullscreen',
                    'insertdatetime media nonbreaking save table contextmenu directionality',
                    'emoticons template paste textcolor colorpicker textpattern imagetools'
                ],
                toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
                toolbar2: 'print preview media | forecolor backcolor emoticons',
                image_advtab: true
            });
            tinymce.suffix = ".min";
            tinyMCE.baseURL = '{{asset('assets/backend/plugins/tinymce')}}';
        });
    </script>

@endpush
