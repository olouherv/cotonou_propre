@extends('layouts.backend.app')

@section('title','Publications')

@push('css')
    <!-- JQuery DataTable Css -->
    <link href="{{asset('assets/backend/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css')}}" rel="stylesheet">
@endpush


@section('content')
    <div class="container-fluid">
            <div class="block-header">
                <a class="btn btn-primary waves-effect" href="{{route('dashboard.vospublications.create')}}">
                    <i class="material-icons">add</i>
                    <span>Nouvelle Publication</span>
                </a>
            </div>
            <!-- Exportable Table -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                TOUTES LES PUBLICATIONS
                                <span class="badge bg-blue">{{$publications->count()}}</span>
                            </h2>

                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>MEDIA</th>
                                        <th>TITRE</th>
                                        <th>AUTEUR</th>
                                        <th>LIEU</th>
                                        <th>TYPE</th>
                                        <th>ACTION</th>
                                    </tr>
                                    </thead>
                                    <tfoot>
                                    <tr>
                                        <th>ID</th>
                                        <th>MEDIA</th>
                                        <th>TITRE</th>
                                        <th>AUTEUR</th>
                                        <th>LIEU</th>
                                        <th>TYPE</th>
                                        <th>ACTION</th>
                                    </tr>
                                    </tfoot>
                                    <tbody>

                                        @foreach($publications as $key=>$publication)
                                            <tr>
                                                <td>{{$key + 1}}</td>
                                                <td align="center">
                                                    @if($publication->mediaurl)
                                                        @if(str_contains($publication->mediaurl,['jpeg','jpg','png']))
                                                            <img src="{{asset($publication->mediaurl)}}" width="50" height="50"/>
                                                        @elseif(str_contains($publication->mediaurl,['avi','mp4','flv']))
                                                            <video src="{{asset($publication->mediaurl)}}" width="50" height="50" controls/>
                                                        @endif
                                                    @endif
                                                </td>
                                                <td>{{$publication->titre}}</td>
                                                <td>{{$publication->utilisateur->name}}</td>
                                                <td>

                                                    @if($publication->quartier_id==0 && $publication->zone_id==0 && $publication->arrondissement_id==0)

                                                    @else
                                                        {{$publication->zone->name}}<br>{{$publication->zone->quartier->name}}<br>{{$publication->zone->quartier->arrondissement->name}}
                                                    @endif

                                                </td>
                                                <td>{{$publication->typepublication->name}}</td>
                                                <td class="text-center">
                                                    <a class="btn btn-info waves-effect" href="{{route('dashboard.vospublications.edit',$publication->id)}}">
                                                        <i class="material-icons">edit</i>

                                                    </a>

                                                    <button class="btn btn-danger waves-effect"
                                                            onclick="deletePublication({{$publication->id}})"
                                                            Publication="button">
                                                        <i class="material-icons">delete</i>
                                                    </button>

                                                    <form method="post" id="delete-form-{{$publication->id}}" action="{{route('dashboard.vospublications.delete',$publication->id)}}" style="display: none;">
                                                        @csrf
                                                        {{--@method('DELETE')--}}
                                                    </form>

                                                </td>
                                            </tr>
                                        @endforeach

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Exportable Table -->
        </div>
@endsection


@push('js')
    <!-- Jquery DataTable Plugin Js -->
    <script src="{{asset('assets/backend/plugins/jquery-datatable/jquery.dataTables.js')}}"></script>
    <script src="{{asset('assets/backend/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js')}}"></script>
    <script src="{{asset('assets/backend/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('assets/backend/plugins/jquery-datatable/extensions/export/buttons.flash.min.js')}}"></script>
    <script src="{{asset('assets/backend/plugins/jquery-datatable/extensions/export/jszip.min.js')}}"></script>
    <script src="{{asset('assets/backend/plugins/jquery-datatable/extensions/export/pdfmake.min.js')}}"></script>
    <script src="{{asset('assets/backend/plugins/jquery-datatable/extensions/export/vfs_fonts.js')}}"></script>
    <script src="{{asset('assets/backend/plugins/jquery-datatable/extensions/export/buttons.html5.min.js')}}"></script>
    <script src="{{asset('assets/backend/plugins/jquery-datatable/extensions/export/buttons.print.min.js')}}"></script>
    <script src="{{asset('assets/backend/js/pages/tables/jquery-datatable.js')}}"></script>

    <script src="https://unpkg.com/sweetalert@2.1.0/dist/sweetalert.min.js"></script>

    <script Publication="text/javascript">
        function deletePublication(id) {
            swal({
                title: "Are you sure?",
                text: "Once deleted, you will not be able to recover this imaginary file!",
                icon: "warning",
                confirmButtonText: 'Oui, supprimer ce Publication!',
                cancelButtonText: 'Non, annuler!',
                cancelButtonClass: 'btn btn-danger',
                confirmButtonClass:'btn btn-success',
                buttons: true,
                dangerMode: true,
            })
                .then((willDelete) => {
                    if (willDelete) {
                        event.preventDefault();
                        document.getElementById('delete-form-'+id).submit();
                        swal("Poof! Your imaginary file has been deleted!", {
                            icon: "success",
                        });
                    } else {
                        swal('Cancelled',
                            "Your imaginary file is safe!",
                            'error');
                    }
                });
        }

    </script>

@endpush
