@extends('layouts.backend.app')

@section('title','Nouvel évènement')

@push('css')

    <!-- Bootstrap Select Css -->
    <link href="{{asset('assets/backend/plugins/bootstrap-select/css/bootstrap-select.css')}}" rel="stylesheet" />

@endpush


@section('content')
    <div class="container-fluid">
        <form action="{{route('dashboard.evenements.store')}}" method="POST" enctype="multipart/form-data">
        @csrf
            <!-- Vertical Layout -->
            <div class="row clearfix">
                <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Planifier un évènement
                            </h2>
                        </div>
                        <div class="body">

                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" id="name" class="form-control" name="titre">
                                    <label class="form-label">Intitulé de l'évènement</label>
                                </div>
                            </div>

                            <div class="form-group form-float">
                                <div class="form-line">
                                    <textarea type="text" id="name" class="form-control" name="description"></textarea>
                                    <label class="form-label">Description de l'évènement</label>
                                </div>
                            </div>
                            <div class="row clearfix">

                                <div class="col-md-6">
                                    <b>Date de début</b>
                                    <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">date_range</i>
                                            </span>
                                        <div class="form-line">
                                            <input type="text" class="form-control datetime" name="datedebut" placeholder="Ex: 24/07/2018 23:59">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <b>Date de fin</b>
                                    <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">date_range</i>
                                            </span>
                                        <div class="form-line">
                                            <input type="text" class="form-control datetime" name ="datefin" placeholder="Ex: 24/07/2018 23:59">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                INFORMATION
                            </h2>
                        </div>
                        <div class="body">
                            <div class="form-group bootstrap-select form-control show-tick">
                                <select class="form-control show-tick" name="typeevenement_id">
                                    <option value="">Choisir le type de l'évènement</option>
                                    @foreach($types as $type)
                                        <option value="{{$type->id}}">{{$type->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <br>
                            <div class="form-group bootstrap-select form-control show-tick">
                                <select class="form-control show-tick" name="zone_id">
                                    <option value="">Choisir la zone</option>
                                    @foreach($arrondissements as $arrondissement)
                                        <optgroup label="{{$arrondissement->name}}">
                                            @foreach($zones as $zone)
                                                @if($zone->quartier->arrondissement->name == $arrondissement->name)
                                                    <option value="{{$zone->id}}">{{$zone->name}}\{{$zone->quartier->name}}</option>
                                                @endif
                                            @endforeach
                                        </optgroup>
                                    @endforeach
                                </select>
                            </div>

                            <a class="btn btn-danger m-t-15 waves-effect" href="{{route('dashboard.evenements')}}">RETOUR</a>
                            <button type="submit" class="btn btn-primary m-t-15 waves-effect">ENREGISTRER</button>
                        </div>
                    </div>
                </div>
            </div>

        </form>
    </div>


@endsection


@push('js')
    <!-- Jquery DataTable Plugin Js -->
    <script src="{{asset('assets/backend/plugins/tinymce/tinymce.js')}}"></script>
    <!-- Select Plugin Js -->
    <script src="{{asset('assets/backend/plugins/bootstrap-select/js/bootstrap-select.js')}}"></script>

@endpush
