@extends('layouts.backend.app')

@section('title','Evenement')

@push('css')
    <!-- JQuery DataTable Css -->
    <link href="{{asset('assets/backend/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css')}}" rel="stylesheet">
@endpush


@section('content')
    <div class="container-fluid">
        @if(Auth::user()->role_id == 3)
            <div class="block-header">
                <a class="btn btn-primary waves-effect" href="{{route('dashboard.evenements.create')}}">
                    <i class="material-icons">add</i>
                    <span>Nouvel Evenement</span>
                </a>
            </div>
        @endif
        <!-- Exportable Table -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            TOUS LES EVENEMENTS
                            <span class="badge bg-blue">{{$evenements->count()}}</span>
                        </h2>

                    </div>
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>TITRE</th>
                                    <th>ORGANISATEUR</th>
                                    <th>LIEU</th>
                                    <th>TYPE</th>
                                    <th>STATUS</th>
                                    <th>ACTION</th>
                                </tr>
                                </thead>
                                <tfoot>
                                <tr>
                                    <th>ID</th>
                                    <th>TITRE</th>
                                    <th>ORGANISATEUR</th>
                                    <th>LIEU</th>
                                    <th>TYPE</th>
                                    <th>STATUS</th>
                                    <th>ACTION</th>
                                </tr>
                                </tfoot>
                                <tbody>

                                    @foreach($evenements as $key=>$evenement)
                                        @if(Auth::id() == $evenement->zone->responsable_id)
                                            <tr>
                                            <td>{{$key + 1}}</td>
                                            <td>{{$evenement->titre}}</td>
                                            <td>{{$evenement->utilisateur->name}}</td>
                                            <td>
                                                    {{$evenement->zone->name}}<br>{{$evenement->zone->quartier->name}}<br>{{$evenement->zone->quartier->arrondissement->name}}
                                            </td>
                                            <td>{{$evenement->typeevenement->name}}</td>
                                            <td>
                                                @if($evenement->status==1)
                                                    <span class="label bg-green">Autorisé</span>
                                                @elseif($evenement->status==0)
                                                    <span class="label bg-red">Refusé</span>
                                                @elseif($evenement->status==2)
                                                    <span class="label bg-blue">En attente</span>
                                                @endif
                                            </td>
                                            <td class="text-center">
                                                @if(Auth::user()->role_id==3 || Auth::user()->role_id==2)
                                                    <a class="btn btn-info waves-effect" href="{{route('dashboard.evenements.edit',$evenement->id)}}">
                                                        <i class="material-icons">{{(Auth::user()->role_id==3)?'edit':'visibility'}}</i>
                                                    </a>
                                                @endif
                                            </td>
                                        </tr>
                                        @elseif(Auth::id() == $evenement->utilisateur_id)
                                            <tr>
                                                <td>{{$key + 1}}</td>
                                                <td>{{$evenement->titre}}</td>
                                                <td>{{$evenement->utilisateur->name}}</td>
                                                <td>
                                                    {{$evenement->zone->name}}<br>{{$evenement->zone->quartier->name}}<br>{{$evenement->zone->quartier->arrondissement->name}}
                                                </td>
                                                <td>{{$evenement->typeevenement->name}}</td>
                                                <td>
                                                    @if($evenement->status==1)
                                                        <span class="label bg-green">Autorisé</span>
                                                    @elseif($evenement->status==0)
                                                        <span class="label bg-red">Refusé</span>
                                                    @elseif($evenement->status==2)
                                                        <span class="label bg-blue">En attente</span>
                                                    @endif
                                                </td>
                                                <td class="text-center">
                                                    <a class="btn btn-info waves-effect" href="{{route('dashboard.evenements.edit',$evenement->id)}}">
                                                        <i class="material-icons">edit</i>
                                                    </a>
                                                    <button class="btn btn-danger waves-effect"
                                                            onclick="deleteEvenement({{$evenement->id}})"
                                                            Publication="button">
                                                        <i class="material-icons">delete</i>
                                                    </button>

                                                    <form method="post" id="delete-form-{{$evenement->id}}" action="{{route('dashboard.evenements.delete',$evenement->id)}}" style="display: none;">
                                                        @csrf
                                                        @method('DELETE')
                                                    </form>
                                                </td>
                                            </tr>
                                        @elseif(Auth::user()->role_id == 1)
                                            <tr>
                                                <td>{{$key + 1}}</td>
                                                <td>{{$evenement->titre}}</td>
                                                <td>{{$evenement->utilisateur->name}}</td>
                                                <td>
                                                    {{$evenement->zone->name}}<br>{{$evenement->zone->quartier->name}}<br>{{$evenement->zone->quartier->arrondissement->name}}
                                                </td>
                                                <td>{{$evenement->typeevenement->name}}</td>
                                                <td>
                                                    @if($evenement->status==1)
                                                        <span class="label bg-green">Autorisé</span>
                                                    @elseif($evenement->status==0)
                                                        <span class="label bg-red">Refusé</span>
                                                    @elseif($evenement->status==2)
                                                        <span class="label bg-blue">En attente</span>
                                                    @endif

                                                </td>
                                                <td class="text-center">
                                                    <button class="btn btn-danger waves-effect"
                                                            onclick="deleteEvenement({{$evenement->id}})"
                                                            Publication="button">
                                                        <i class="material-icons">delete</i>
                                                    </button>

                                                    <form method="post" id="delete-form-{{$evenement->id}}" action="{{route('dashboard.evenements.delete',$evenement->id)}}" style="display: none;">
                                                        @csrf
                                                        @method('DELETE')
                                                    </form>
                                                </td>
                                            </tr>
                                        @endif
                                    @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Exportable Table -->
    </div>
@endsection


@push('js')
    <!-- Jquery DataTable Plugin Js -->
    <script src="{{asset('assets/backend/plugins/jquery-datatable/jquery.dataTables.js')}}"></script>
    <script src="{{asset('assets/backend/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js')}}"></script>
    <script src="{{asset('assets/backend/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('assets/backend/plugins/jquery-datatable/extensions/export/buttons.flash.min.js')}}"></script>
    <script src="{{asset('assets/backend/plugins/jquery-datatable/extensions/export/jszip.min.js')}}"></script>
    <script src="{{asset('assets/backend/plugins/jquery-datatable/extensions/export/pdfmake.min.js')}}"></script>
    <script src="{{asset('assets/backend/plugins/jquery-datatable/extensions/export/vfs_fonts.js')}}"></script>
    <script src="{{asset('assets/backend/plugins/jquery-datatable/extensions/export/buttons.html5.min.js')}}"></script>
    <script src="{{asset('assets/backend/plugins/jquery-datatable/extensions/export/buttons.print.min.js')}}"></script>
    <script src="{{asset('assets/backend/js/pages/tables/jquery-datatable.js')}}"></script>

    <script src="https://unpkg.com/sweetalert@2.1.0/dist/sweetalert.min.js"></script>

    <script Publication="text/javascript">
        function deleteEvenement(id) {
            swal({
                title: "Are you sure?",
                text: "Once deleted, you will not be able to recover this imaginary file!",
                icon: "warning",
                cancelButtonClass: 'btn btn-danger',
                confirmButtonClass:'btn btn-success',
                buttons: true,
                dangerMode: true,
            })
                .then((willDelete) => {
                    if (willDelete) {
                        event.preventDefault();
                        document.getElementById('delete-form-'+id).submit();
                        swal("Poof! Your imaginary file has been deleted!", {
                            icon: "success",
                        });
                    } else {
                        swal('Cancelled',
                            "Your imaginary file is safe!",
                            'error');
                    }
                });
        }

    </script>

@endpush
