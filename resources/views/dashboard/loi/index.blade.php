@extends('layouts.backend.app')

@section('title','Lois')

@push('css')
    <!-- JQuery DataTable Css -->
    <link href="{{asset('assets/backend/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css')}}" rel="stylesheet">
@endpush


@section('content')
    <div class="container-fluid">
            <div class="block-header">
                <a class="btn btn-primary waves-effect" href="{{route('dashboard.loi.create')}}">
                    <i class="material-icons">add</i>
                    <span>Nouvelle Loi</span>
                </a>
            </div>
            <!-- Exportable Table -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                TOUTES LES LOIS
                            </h2>

                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>TITRE</th>
                                        <th>RESPONSABLE</th>
                                        <th>LIEU</th>
                                        <th>TYPE</th>
                                        <th>ACTION</th>
                                    </tr>
                                    </thead>
                                    <tfoot>
                                    <tr>
                                        <th>ID</th>
                                        <th>TITRE</th>
                                        <th>RESPONSABLE</th>
                                        <th>LIEU</th>
                                        <th>TYPE</th>
                                        <th>ACTION</th>
                                    </tr>
                                    </tfoot>
                                    <tbody>

                                        @foreach($lois as $key=>$loi)
                                            <tr>
                                                <td>{{$key + 1}}</td>
                                                <td>{{$loi->titre}}</td>
                                                <td>{{$loi->utilisateur->name}}</td>
                                                <td>

                                                    @if($loi->quartier_id==0 && $loi->zone_id==0 && $loi->arrondissement_id==0)
                                                        A toutes les zones
                                                    @else
                                                        {{$loi->zone->name}}<br>{{$loi->zone->quartier->name}}<br>{{$loi->zone->quartier->arrondissement->name}}
                                                    @endif

                                                </td>
                                                <td>{{$loi->typeloi->name}}</td>
                                                <td class="text-center">
                                                    <a class="btn btn-info waves-effect" href="{{route('dashboard.loi.edit',$loi->id)}}">
                                                        <i class="material-icons">edit</i>

                                                    </a>

                                                    <button class="btn btn-danger waves-effect"
                                                            onclick="deleteLoi({{$loi->id}})"
                                                            Loi="button">
                                                        <i class="material-icons">delete</i>
                                                    </button>

                                                    <form method="post" id="delete-form-{{$loi->id}}" action="{{route('dashboard.loi.destroy',$loi->id)}}" style="display: none;">
                                                        @csrf
                                                        @method('DELETE')
                                                    </form>

                                                </td>
                                            </tr>
                                        @endforeach

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Exportable Table -->
        </div>
@endsection


@push('js')
    <!-- Jquery DataTable Plugin Js -->
    <script src="{{asset('assets/backend/plugins/jquery-datatable/jquery.dataTables.js')}}"></script>
    <script src="{{asset('assets/backend/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js')}}"></script>
    <script src="{{asset('assets/backend/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('assets/backend/plugins/jquery-datatable/extensions/export/buttons.flash.min.js')}}"></script>
    <script src="{{asset('assets/backend/plugins/jquery-datatable/extensions/export/jszip.min.js')}}"></script>
    <script src="{{asset('assets/backend/plugins/jquery-datatable/extensions/export/pdfmake.min.js')}}"></script>
    <script src="{{asset('assets/backend/plugins/jquery-datatable/extensions/export/vfs_fonts.js')}}"></script>
    <script src="{{asset('assets/backend/plugins/jquery-datatable/extensions/export/buttons.html5.min.js')}}"></script>
    <script src="{{asset('assets/backend/plugins/jquery-datatable/extensions/export/buttons.print.min.js')}}"></script>
    <script src="{{asset('assets/backend/js/pages/tables/jquery-datatable.js')}}"></script>

    <script src="https://unpkg.com/sweetalert@2.1.0/dist/sweetalert.min.js"></script>

    <script>
        function deleteLoi(id) {
            swal({
                title: "Are you sure?",
                text: "Once deleted, you will not be able to recover this imaginary file!",
                icon: "warning",
                confirmButtonText: 'Oui, supprimer cette Loi!',
                cancelButtonText: 'Non, annuler!',
                cancelButtonClass: 'btn btn-danger',
                confirmButtonClass:'btn btn-success',
                buttons: true,
                dangerMode: true,
            })
                .then((willDelete) => {
                    if (willDelete) {
                        event.preventDefault();
                        document.getElementById('delete-form-'+id).submit();
                        swal("Poof! Your imaginary file has been deleted!", {
                            icon: "success",
                        });
                    } else {
                        swal('Cancelled',
                            "Your imaginary file is safe!",
                            'error');
                    }
                });
        }

    </script>

@endpush
