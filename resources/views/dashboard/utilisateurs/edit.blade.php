@extends('layouts.backend.app')

@section('title',"Modifier information de l'utilisateur")

@push('css')

    <!-- Bootstrap Select Css -->
    <link href="{{asset('assets/backend/plugins/bootstrap-select/css/bootstrap-select.css')}}" rel="stylesheet" />

@endpush


@section('content')
    <div class="container-fluid">
        <form action="{{route('dashboard.utilisateurs.update',$utilisateur->id)}}" method="POST" enctype="multipart/form-data">
        @csrf
        @method('PUT')
        <!-- Vertical Layout -->
            <div class="row clearfix">
                <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Modifier information de l'utilisateur
                            </h2>
                        </div>
                        <div class="body">

                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" id="name" class="form-control" name="name" value="{{$utilisateur->name}}">
                                    <label class="form-label">Nom complet</label>
                                </div>
                            </div>

                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" id="username" class="form-control" name="username" value="{{$utilisateur->username}}">
                                    <label class="form-label">Nom d'utilisateur</label>
                                </div>
                            </div>

                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="email" id="email" class="form-control" name="email" value="{{$utilisateur->email}}">
                                    <label class="form-label">Email</label>
                                </div>
                            </div>

                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="tel" id="telephone" class="form-control" name="telephone" value="{{$utilisateur->telephone}}">
                                    <label class="form-label">Telephone</label>
                                </div>
                            </div>

                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="password" id="email" class="form-control" name="password">
                                    <label class="form-label">Mot de passe</label>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="form-line">
                                    <textarea rows="5" id="about" class="form-control" name="about" placeholder="A propos de l'utilisateur">{{$utilisateur->about}}"</textarea>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Plus d'Information
                            </h2>
                        </div>
                        <div class="body">
                            <div class="form-group bootstrap-select form-control show-tick">
                                <select class="form-control show-tick" name="role_id" id="roles">
                                    <option value="">Role de l'utilisateur</option>
                                    @foreach($roles as $role)
                                        <option value="{{$role->id}}" {{($utilisateur->role_id==$role->id)?'selected':''}}>{{$role->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <br>
                            <div id="responsable" class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" id="name" class="form-control" name="responsable" value="{{$utilisateur->responsable}}">
                                    <label class="form-label">Nom du responsable</label>
                                </div>
                            </div>


                            <a class="btn btn-danger m-t-15 waves-effect" href="{{route('dashboard.utilisateurs.index')}}">RETOUR</a>
                            <button type="submit" class="btn btn-primary m-t-15 waves-effect">ENREGISTRER</button>
                        </div>
                    </div>
                </div>
            </div>

        </form>
    </div>


@endsection


@push('js')
    <!-- Jquery DataTable Plugin Js -->
    <script src="{{asset('assets/backend/plugins/tinymce/tinymce.js')}}"></script>
    <!-- Select Plugin Js -->
    <script src="{{asset('assets/backend/plugins/bootstrap-select/js/bootstrap-select.js')}}"></script>

    <script>
        var role = $("#roles").val();
        if(role==3){
            $('#responsable').show();
        }else{
            $('#responsable').hide();
        }
        $('#roles').on('change',function () {
            role = $("#roles").val();
            if(role==3){
                $('#responsable').show();
            }else{
                $('#responsable').hide();
            }
        });

    </script>

@endpush
