@extends('layouts.backend.app')

@section('title','Nouvelle loi')

@push('css')

    <!-- Bootstrap Select Css -->
    <link href="{{asset('assets/backend/plugins/bootstrap-select/css/bootstrap-select.css')}}" rel="stylesheet" />

@endpush


@section('content')
    <div class="container-fluid">
        <form action="{{route('dashboard.voslois.store')}}" method="POST" enctype="multipart/form-data">
        @csrf
            <!-- Vertical Layout -->
            <div class="row clearfix">
                <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Publier une nouvelle loi
                            </h2>
                        </div>
                        <div class="body">

                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" id="name" class="form-control" name="titre">
                                    <label class="form-label">Titre de la loi</label>
                                </div>
                            </div>

                            <div class="form-group form-float">
                                <div class="form-line">
                                    <textarea type="text" id="name" class="form-control" name="description"></textarea>
                                    <label class="form-label">Description de la loi</label>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="pdf">Joindre un fichier pdf</label>
                                <input type="file" name="pdf">
                            </div>

                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                INFORMATION
                            </h2>
                        </div>
                        <div class="body">
                            <div class="form-group bootstrap-select form-control show-tick">
                                <select class="form-control show-tick" name="typeloi_id">
                                    <option value="">Choisir le type de loi</option>
                                    @foreach($types as $type)
                                        <option value="{{$type->id}}">{{$type->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <br>
                            <div class="form-group bootstrap-select form-control show-tick">
                                <select class="form-control show-tick" name="zone_id">
                                    <option value="0">Toutes les zones</option>
                                    @foreach($arrondissements as $arrondissement)
                                        <optgroup label="{{$arrondissement->name}}">
                                            @foreach($zones as $zone)
                                                @if($zone->quartier->arrondissement->name == $arrondissement->name)
                                                    <option value="{{$zone->id}}">{{$zone->name}}\{{$zone->quartier->name}}</option>
                                                @endif
                                            @endforeach
                                        </optgroup>
                                    @endforeach
                                </select>
                            </div>

                            <a class="btn btn-danger m-t-15 waves-effect" href="{{route('dashboard.voslois.index')}}">RETOUR</a>
                            <button type="submit" class="btn btn-primary m-t-15 waves-effect">ENREGISTRER</button>
                        </div>
                    </div>
                </div>
            </div>

        </form>
    </div>


@endsection


@push('js')
    <!-- Jquery DataTable Plugin Js -->
    <script src="{{asset('assets/backend/plugins/tinymce/tinymce.js')}}"></script>
    <!-- Select Plugin Js -->
    <script src="{{asset('assets/backend/plugins/bootstrap-select/js/bootstrap-select.js')}}"></script>

@endpush
