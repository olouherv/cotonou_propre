@extends('layouts.frontend.app')

@section('title','Accueil')

@push('css')
    <link href="{{asset('assets/frontend/css/home/responsive.css')}}" rel="stylesheet">
    <link href="{{asset('assets/frontend/css/home/styles.css')}}" rel="stylesheet">
    <style>
        .like_publications{
            color:green;
        }

    </style>
@endpush

@section('content')
    <div class="slider"></div><!-- slider -->

    <section class="blog-area section">
        <div class="container">
            <div class="row">
            <div class="col-lg-9 col-md-9 pull-left">
                    <div class="row">
                        @foreach($publications as $publication)

                            <div class="col-lg-6 col-md-6">
                                <div class="card h-100">
                                    <div class="single-post post-style-1">

                                        <div class="blog-image">
                                            @if(str_contains($publication->mediaurl,['jpeg','jpg','png']))
                                                <img src="{{asset($publication->mediaurl)}}" alt="Blog Image">
                                            @elseif(str_contains($publication->mediaurl,['avi','mp4','flv']))
                                                <video src="{{asset($publication->mediaurl)}}" controls/>
                                            @endif
                                        </div>

                                        <a class="avatar" href="#"><img src="{{asset($publication->utilisateur->image)}}" alt="Profile Image"></a>
                                        <div class="blog-info">

                                            <h4 class="title"><a href="{{route('publications.details',$publication->slug)}}"><b>{{$publication->titre}}</b></a></h4>

                                            <ul class="post-footer">
                                                <li>

                                                    @guest
                                                        <a href="javascript:void(0)" onclick="toastr.info('Vous devez être connecté','Info',{
                                                    closeButton: true,
                                                    progressBar: true,
                                                })"><i class="ion-heart"></i>{{$publication->like_utilisateurs->count()}}</a>
                                                    @else
                                                        <a href="{{route('publications.like',$publication->id)}}"
                                                           class="{{(!Auth::user()->like_publications->where('pivot.publication_id',$publication->id)->count()==0)?'like_publications':''}}">
                                                            <i class="ion-heart"></i>{{$publication->like_utilisateurs->count()}}
                                                        </a>
                                                    @endguest

                                                </li>
                                                <li><a><i class="ion-chatbubble"></i>{{$publication->commentaires->count()}}</a></li>
                                                <li><a><i class="ion-eye"></i>{{$publication->nbrevues}}</a></li>
                                            </ul>

                                        </div><!-- blog-info -->
                                    </div><!-- single-post -->
                                </div><!-- card -->
                            </div><!-- col-lg-4 col-md-6 -->

                        @endforeach
                    </div>
            </div>

            <div class="col-md-3 col-lg-3 pull-right" style="text-align: left; background: white;">
                <div>
                    <h4 style="margin-bottom: 10px;"><b>Type de publications</b></h4>
                    <hr>
                    <ol class="list-unstyled">
                        <li><a class="btn btn-default" href="{{route('publications')}}" style="background: whitesmoke;">TOUT</a></li>
                        @foreach($typepublications as $type)
                            <li style="margin: 5px 0 5px 0;"><a class="btn btn-default" href="{{route('publications.type',$type->slug)}}" style="background: whitesmoke;">{{$type->name}}</a></li>
                        @endforeach
                    </ol>
                    <hr>
                    <h4 style="margin-bottom: 10px;"><b>Arrondissements</b></h4>
                    <hr>
                    <ol class="list-unstyled">
                        <li style="margin: 5px 0 5px 0;"><a class="btn btn-default" href="{{route('publications')}}" style="background: whitesmoke;">TOUT</a></li>
                        @foreach($arrondissements as $arrondissement)
                            <li style="margin: 5px 0 5px 0;"><a class="btn btn-default" href="{{route('publications.arrondissement',$arrondissement->slug)}}" style="background: whitesmoke;">{{$arrondissement->name}}</a></li>
                        @endforeach
                    </ol>
                    <hr>
                </div>
            </div>
            </div>
            {{$publications->links()}}

        </div><!-- container -->
    </section><!-- section -->
@endsection

@push('js')
    <script src="{{asset('assets/frontend/js/swiper.js')}}"></script>
@endpush
