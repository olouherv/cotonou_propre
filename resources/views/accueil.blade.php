@extends('layouts.frontend.app')

@section('title','Accueil')

@push('css')
    <link href="{{asset('assets/frontend/css/home/responsive.css')}}" rel="stylesheet">
    <link href="{{asset('assets/frontend/css/home/styles.css')}}" rel="stylesheet">
    <style>
        .like_publications{
            color:green;
        }

    </style>
@endpush

@section('content')

    <div class="main-slider">
        <div class="swiper-container position-static swiper-container-horizontal" data-slide-effect="slide" data-autoheight="false"
             data-swiper-speed="500" data-swiper-autoplay="10000" data-swiper-margin="0" data-swiper-slides-per-view="4"
             data-swiper-breakpoints="true" data-swiper-loop="true">
            <div class="swiper-wrapper">
                @foreach($arrondissements as $arrondissement)
                    <div class="swiper-slide">
                        <a class="slider-category" href="#">
                            <div class="blog-image"><img src="{{asset('assets/frontend/images/category-1-400x250.jpg')}}" alt="Arrondissement Image"></div>

                            <div class="category">
                                <div class="display-table center-text">
                                    <div class="display-table-cell">
                                        <h3><b>{{$arrondissement->name}}</b></h3>
                                    </div>
                                </div>
                            </div>

                        </a>
                    </div><!-- swiper-slide -->
                @endforeach
            </div><!-- swiper-wrapper -->

        </div><!-- swiper-container -->

    </div>

    <section class="blog-area section">
        <div class="container">
            <div class="row">
                <div class="col-lg-9 col-md-9 pull-left">
                    <hr>
                    <h2>Nouvelles publications</h2>
                    <hr>
                    <div id="newpub">
                        <div class="row">
                            @if($pubs_rec->count()>0)
                                @foreach($pubs_rec as $publication)

                                <div class="col-lg-6 col-md-6">
                                    <div class="card h-100">
                                        <div class="single-post post-style-1">

                                            <div class="blog-image">
                                                @if(str_contains($publication->mediaurl,['jpeg','jpg','png']))
                                                    <img src="{{asset($publication->mediaurl)}}" alt="Blog Image">
                                                @elseif(str_contains($publication->mediaurl,['avi','mp4','flv']))
                                                    <video src="{{asset($publication->mediaurl)}}" controls/>
                                                @endif
                                            </div>

                                            <a class="avatar" href="#"><img src="{{asset($publication->utilisateur->image)}}" alt="Profile Image"></a>
                                            <div class="blog-info">

                                                <h4 class="title"><a href="{{route('publications.details',$publication->slug)}}"><b>{{$publication->titre}}</b></a></h4>

                                                <ul class="post-footer">
                                                    <li>

                                                        @guest
                                                            <a href="javascript:void(0)" onclick="toastr.info('Vous devez être connecté','Info',{
                                                        closeButton: true,
                                                        progressBar: true,
                                                    })"><i class="ion-heart"></i>{{$publication->like_utilisateurs->count()}}</a>
                                                        @else
                                                            <a href="{{route('publications.like',$publication->id)}}"
                                                               class="{{(!Auth::user()->like_publications->where('pivot.publication_id',$publication->id)->count()==0)?'like_publications':''}}">
                                                                <i class="ion-heart"></i>{{$publication->like_utilisateurs->count()}}
                                                            </a>
                                                        @endguest

                                                    </li>
                                                    <li><a><i class="ion-chatbubble"></i>{{$publication->commentaires->count()}}</a></li>
                                                    <li><a><i class="ion-eye"></i>{{$publication->nbrevues}}</a></li>
                                                </ul>

                                            </div><!-- blog-info -->
                                        </div><!-- single-post -->
                                    </div><!-- card -->
                                </div><!-- col-lg-4 col-md-6 -->

                            @endforeach
                            @else
                                Aucune publication
                            @endif
                        </div>
                        @if($pubs_rec->count()==4)
                            <a class="load-more-btn" href="{{route('publications')}}"><b>VOIR PLUS</b></a>
                        @endif
                    </div>
                    <hr>
                    <h2>Nouveaux évènements</h2>
                    <hr>
                    <div id="newevents">
                    <div class="row">
                        @if($evts_rec->count()>0)
                            @foreach($evts_rec as $evenement)
                                <div class="col-lg-12 col-md-12">
                                    <div class="card h-100">
                                        <div class="single-post post-style-2">
                                            <div style="padding: 30px;">
                                                <h6 class="pre-title"><a href="#"><b>{{$evenement->typeevenement->name}}</b></a></h6>

                                                <h4 class="title"><a href="#"><b>{{$evenement->titre}}</b> - {{$evenement->zone->name}}\{{$evenement->zone->quartier->name}}\{{$evenement->zone->quartier->arrondissement->name}}</a></h4>

                                                <p>{{$evenement->description}}</p>

                                                <div class="avatar-area">
                                                    <a class="avatar" href="#"><img src="{{asset($evenement->utilisateur->image)}}" alt="Profile Image"></a>
                                                    <div class="right-area">
                                                        <a class="name" href="#"><b>{{$evenement->utilisateur->name}}</b></a>
                                                        <h6 class="date" href="#">publié le : {{$evenement->created_at}}</h6>
                                                    </div>
                                                </div>

                                            </div>
                                            <ul class="post-footer">
                                                <li style="width: 50%;"><a><i class="ion-calendar"></i>{{$evenement->datedebut}}</a></li>
                                                <li style="width: 50%;"><a><i class="ion-calendar"></i>{{$evenement->datefin}}</a></li>
                                            </ul>
                                        </div><!-- single-post extra-blog -->
                                    </div><!-- card -->
                                </div><!-- col-lg-12 col-md-12 -->
                            @endforeach
                        @else
                            Aucun évènement
                        @endif
                    </div>
                    @if($evts_rec->count()==3)
                        <a class="load-more-btn" href="{{route('evenements')}}"><b>VOIR PLUS</b></a>
                    @endif
                    </div>
                    <hr>
                    <h2>Nouvelles lois</h2>
                    <hr>
                    <div id="newlois">
                    <div class="row">
                        @if($lois_rec->count()>0)
                            @foreach($lois_rec as $loi)
                            <div class="col-lg-12 col-md-12">
                                <div class="card h-100">
                                    <div class="single-post post-style-2">
                                        <div style="padding: 30px;">
                                            <h6 class="pre-title"><a href="#"><b>{{$loi->typeloi->name}}</b></a></h6>

                                            <h4 class="title"><a href="#"><b>{{$loi->titre}}</b> -
                                                    @if($loi->zone!=0)
                                                        {{$loi->zone->name}}\{{$loi->zone->quartier->name}}\{{$loi->zone->quartier->arrondissement->name}}</a></h4>
                                            @else
                                                Toutes les zones
                                            @endif
                                            <p>{{$loi->description}}</p>

                                            <div class="avatar-area">
                                                <a class="avatar" href="#"><img src="{{asset($loi->utilisateur->image)}}" alt="Profile Image"></a>
                                                <div class="right-area">
                                                    <a class="name" href="#"><b>{{$loi->utilisateur->name}}</b></a>
                                                    <h6 class="date" href="#">publié le : {{$loi->created_at}}</h6>
                                                </div>
                                            </div>

                                        </div>
                                        <ul class="post-footer">
                                            <li>
                                                @guest
                                                    <a href="javascript:void(0)" onclick="toastr.info('Vous devez être connecté','Info',{
                                                closeButton: true,
                                                progressBar: true,
                                            })"><i class="ion-document"></i>Telecharger le document</a>
                                                @else
                                                    <a href="{{asset($loi->pdfurl)}}"><i class="ion-document">Telecharger le document</i></a>

                                            @endguest
                                        </ul>
                                    </div><!-- single-post extra-blog -->

                                </div><!-- card -->
                            </div><!-- col-lg-12 col-md-12 -->
                        @endforeach
                        @else
                            Aucune loi
                        @endif
                    </div>
                    @if($lois_rec->count()==3)
                        <a class="load-more-btn" href="{{route('lois')}}"><b>VOIR PLUS</b></a>
                    @endif
                    </div>
                </div>

            <div class="col-md-3 col-lg-3 pull-right" style="text-align: left; background: white;">
                <div>
                    <h4 style="margin-bottom: 10px;"><b>Publications les plus vues</b></h4>
                    <hr>
                    <ol class="list-unstyled">
                        @foreach($pubs_vues as $pub)
                            <li style="margin: 5px 0 5px 0;"><a class="btn btn-default" href="{{route('publications.details',$pub->slug)}}" style="background: whitesmoke;">{{str_limit($pub->titre,28)}}</a></li>
                        @endforeach
                    </ol>
                    <hr>
                    <h4 style="margin-bottom: 10px;"><b>(05) Evenements à venir</b></h4>
                    <hr>
                    <ol class="list-unstyled">
                        @foreach($evts_next as $evt)
                            <li style="margin: 5px 0 5px 0;"><a class="btn btn-default" {{--href="{{route('publications.arrondissement',$arrondissement->slug)}}"--}} style="background: whitesmoke;">{!!$evt->datedebut.' :<br> '.str_limit($evt->titre,28)!!}</a></li>
                        @endforeach
                    </ol>
                    <hr>
                </div>
            </div>
            </div>

        </div><!-- container -->
    </section><!-- section -->
@endsection

@push('js')
<script>
    $(document).ready(function () {
        setInterval(function () {
            $(" #newpub ").load('{{route('ajax.newpub')}}');
            $(" #newevents ").load('{{route('ajax.newevents')}}');
            $(" #newlois ").load('{{route('ajax.newlois')}}');
        },10000);
    });
</script>
@endpush
