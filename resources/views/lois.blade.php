@extends('layouts.frontend.app')

@section('title','Lois')

@push('css')

    <link href="{{asset('assets/frontend/css/evenements/responsive.css')}}" rel="stylesheet">
    <link href="{{asset('assets/frontend/css/evenements/styles.css')}}" rel="stylesheet">
    <style>
        .like_publications{
            color:green;
        }

        {{--.header-bg{
            height: 400px;
            width: 100%;
            background-image: url("{{asset($publication->mediaurl)}}");
        }--}}

    </style>

@endpush

@section('content')
    <section class="blog-area section">
        <div class="container">

            <div class="row">
                <div class="col-md-9 col-lg-9 pull-left">
                    @foreach($lois as $loi)
                        <div style="margin-bottom: 30px;">
                            <div class="card h-100">
                                <div class="single-post post-style-2">
                                    <div style="padding: 30px;">
                                        <h6 class="pre-title"><a href="#"><b>{{$loi->typeloi->name}}</b></a></h6>

                                        <h4 class="title"><a href="#"><b>{{$loi->titre}}</b> -
                                                @if($loi->zone!=0)
                                                    {{$loi->zone->name}}\{{$loi->zone->quartier->name}}\{{$loi->zone->quartier->arrondissement->name}}</a></h4>
                                                @else
                                                    Toutes les zones
                                                @endif
                                        <p>{{$loi->description}}</p>

                                        <div class="avatar-area">
                                            <a class="avatar" href="#"><img src="{{asset($loi->utilisateur->image)}}" alt="Profile Image"></a>
                                            <div class="right-area">
                                                <a class="name" href="#"><b>{{$loi->utilisateur->name}}</b></a>
                                                <h6 class="date" href="#">publié le : {{$loi->created_at}}</h6>
                                            </div>
                                        </div>

                                    </div>
                                    <ul class="post-footer">
                                        <li>
                                            @guest
                                                <a href="javascript:void(0)" onclick="toastr.info('Vous devez être connecté','Info',{
                                                closeButton: true,
                                                progressBar: true,
                                            })"><i class="ion-document"></i>Telecharger le document</a>
                                            @else
                                                <a href="{{asset($loi->pdfurl)}}"><i class="ion-document">Telecharger le document</i></a>

                                            @endguest
                                    </ul>
                                </div><!-- single-post extra-blog -->

                            </div><!-- card -->
                        </div><!-- col-lg-12 col-md-12 -->
                    @endforeach
                </div>
                <div class="col-md-3 col-lg-3 pull-right" style="text-align: left; background: white;">
                    <div>
                        <h4 style="margin-bottom: 10px;"><b>Type de lois</b></h4>
                        <ol class="list-unstyled">
                            <li><a class="btn btn-default" href="{{route('lois')}}" style="background: whitesmoke;">TOUT</a></li>
                            @foreach($types as $type)
                                <li style="margin: 5px 0 5px 0;"><a class="btn btn-default" href="{{route('lois.type',$type->slug)}}" style="background: whitesmoke;">{{$type->name}}</a></li>
                            @endforeach
                        </ol>
                        <hr>
                        <h4 style="margin-bottom: 10px;"><b>Arrondissements</b></h4>
                        <hr>
                        <ol class="list-unstyled">
                            <li style="margin: 5px 0 5px 0;"><a class="btn btn-default" href="{{route('lois')}}" style="background: whitesmoke;">TOUT</a></li>
                            @foreach($arrondissements as $arrondissement)
                                <li style="margin: 5px 0 5px 0;"><a class="btn btn-default" href="{{route('lois.arrondissement',$arrondissement->slug)}}" style="background: whitesmoke;">{{$arrondissement->name}}</a></li>
                            @endforeach
                        </ol>
                        <hr>
                    </div>
                </div>
            </div><!-- row -->
            {{$lois->links()}}

        </div><!-- container -->
    </section>
@endsection

@push('js')
    <script src="{{asset('assets/frontend/js/swiper.js')}}"></script>
@endpush
