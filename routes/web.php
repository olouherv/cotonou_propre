<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','HomeController@index')->name('accueil');
Route::get('publications/type/{slug}','PublicationsController@index')->name('publications.type');
Route::get('publications/arrondissement/{slug}','PublicationsController@arrondissement')->name('publications.arrondissement');
Route::get('publications/all','PublicationsController@index')->name('publications');
Route::get('publications/{slug}','PublicationsController@details')->name('publications.details');
Route::get('publications/type/{type}','PublicationsController@index')->name('publications.type');
Route::get('publications/zone/{zone}','PublicationsController@zone')->name('publications.zone');
Route::get('publications/quartier/{quartier}','PublicationsController@quartier')->name('publications.quartier');
Route::get('publications/arrondissement/{arrondissement}','PublicationsController@arrondissement')->name('publications.arrondissement');
Route::get('ajax/newpub','HomeController@newpub')->name('ajax.newpub');
Route::get('ajax/newevents','HomeController@newevents')->name('ajax.newevents');
Route::get('ajax/newlois','HomeController@newlois')->name('ajax.newlois');

Route::get('evenements','EvenementController@index')->name('evenements');
Route::get('evenements/{slug}','EvenementController@index')->name('evenements.type');
Route::get('evenements/arrondissement/{slug}','EvenementController@arrondissement')->name('evenements.arrondissement');

Route::get('leslois','LoiController@index')->name('lois');
Route::get('leslois/{slug}','LoiController@index')->name('lois.type');
Route::get('leslois/arrondissement/{slug}','LoiController@arrondissement')->name('lois.arrondissement');

Route::get('dashboard','DashboardController@index')->name('dashboard')->middleware('auth');

Auth::routes();

Route::post('inscription','InscriptionController@inscription')->name('inscription');

Route::group(['middleware'=> ['auth']],function (){
    Route::get('publications/{id}/like','PublicationsController@add')->name('publications.like');
    Route::post('commentaire/{id}','CommentaireController@store')->name('commentaire.store');
    Route::get('dashboard/voscommentaires','CommentaireController@voscommentaires')->name('voscommentaires');
    Route::post('dashboard/voscommentaires/delete/{id}','CommentaireController@destroy')->name('voscommentaires.delete');
    Route::get('dashboard/commentaires','CommentaireController@commentaires')->name('commentaires')->middleware('checkadmin');
    Route::post('dashboard/commentaires/delete/{id}','CommentaireController@destroy')->name('commentaires.delete')->middleware('checkadmin');
});

Route::group(['as'=>'dashboard.','prefix'=>'dashboard','namespace'=>'dashboard','middleware'=>['auth','dashboard']], function (){

    Route::resource('typeevenement','TypeevenementController')->middleware('checkadmin');
    Route::resource('typepublication','TypesPublicationController')->middleware('checkadmin');
    Route::resource('typeloi','TypeloiController')->middleware('checkadmin');
    Route::resource('arrondissement','ArrondissementController')->middleware('checkadmin');
    Route::resource('quartier','QuartierController')->middleware('checkadmin');
    Route::resource('zone','ZoneController')->middleware('checkadmin');
    Route::resource('publication','PublicationController')->middleware('checkadmin');
    Route::resource('loi','LoiController')->middleware('checkadmin');
    Route::resource('voslois','VosloiController')->middleware('checkrespoadmin');
    Route::resource('utilisateurs','UtilisateurController')->middleware('checkadmin');
    //Route::resource('evenement','EvenementController')->middleware('checknotcitoyen');
    //Route::resource('vospublication','VospublicationController');

    Route::get('/evenements','EvenementController@index')->name('evenements')->middleware('checknotcitoyen');
    Route::get('/evenements/create','EvenementController@create')->name('evenements.create')->middleware('checkassociation');
    Route::post('/evenements/store','EvenementController@store')->name('evenements.store')->middleware('checkassociation');
    Route::get('/evenements/edit/{id}','EvenementController@edit')->name('evenements.edit')->middleware('checkassorespo');
    Route::post('/evenements/update/{id}','EvenementController@update')->name('evenements.update')->middleware('checkassorespo');
    Route::post('/evenements/delete/{id}','EvenementController@destroy')->name('evenements.delete')->middleware('checkassoadmin');

    Route::get('/vospublications','VospublicationController@index')->name('vospublications');
    Route::get('/vospublications/create','VospublicationController@create')->name('vospublications.create');
    Route::post('/vospublications/store','VospublicationController@store')->name('vospublications.store');
    Route::get('/vospublications/edit/{id}','VospublicationController@edit')->name('vospublications.edit');
    Route::post('/vospublications/update/{id}','VospublicationController@update')->name('vospublications.update');
    Route::post('/vospublications/delete/{id}','VospublicationController@destroy')->name('vospublications.delete');

    Route::get('parametres','ParametreController@index')->name('parametres');
    Route::put('parametres/update','ParametreController@update')->name('parametres.update');
    Route::put('parametres/change','ParametreController@change')->name('parametres.change');

    Route::get('favoris','LikesController@index')->name('favoris');

});
